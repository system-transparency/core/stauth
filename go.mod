module git.glasklar.is/system-transparency/core/stauth

go 1.20

require (
	github.com/cavaliergopher/cpio v1.0.1
	github.com/diskfs/go-diskfs v1.3.0
	github.com/go-chi/chi/v5 v5.0.8
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.9
	github.com/google/go-tpm v0.9.0
	github.com/google/go-tpm-tools v0.4.0
	github.com/google/uuid v1.3.0
	github.com/rs/zerolog v1.29.1
	github.com/saferwall/pe v1.4.2
	github.com/sergi/go-diff v1.3.1
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.8.3
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/edsrzf/mmap-go v1.1.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/pierrec/lz4 v2.3.0+incompatible // indirect
	github.com/pkg/xattr v0.4.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	go.mozilla.org/pkcs7 v0.0.0-20210826202110-33d05740a352 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	gopkg.in/djherbis/times.v1 v1.2.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
