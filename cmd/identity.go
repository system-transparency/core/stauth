package cmd

import (
	"context"
	"errors"
	"fmt"
	"os"

	"git.glasklar.is/system-transparency/core/stauth/attestation"
	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/spf13/cobra"
)

var (
	identityCmd = &cobra.Command{
		Use:     "identity [IDENTITY]",
		Aliases: []string{"id"},
		Short:   "Show and set the device's UX identity",
		Run: func(cmd *cobra.Command, args []string) {
			ctx := cmd.Context()
			err := error(nil)

			switch len(args) {
			case 0:
				err = doShowIdentity(ctx)
			case 1:
				err = doSetIdentity(ctx, args[0])
			default:
				err = errors.New("too many arguments")
			}

			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				os.Exit(1)
			}
		},
	}
)

func doShowIdentity(ctx context.Context) error {
	// connect to TPM
	conn, err := tpm2.OpenTPM(tpmDevicePath)
	if err != nil {
		return err
	}

	// read identity
	id, err := attestation.Identity(ctx, conn)
	if err == attestation.ErrUnprovisioned {
		fmt.Printf("No identity set. Use 'stauth identity <IDENTITY>' to set one.\n")
	} else if err != nil {
		return err
	}

	fmt.Printf("Identity: %s\n", id)

	return nil
}

func doSetIdentity(ctx context.Context, identity string) error {
	// connect to TPM
	conn, err := tpm2.OpenTPM(tpmDevicePath)
	if err != nil {
		return err
	}

	if identity == "" {
		err = attestation.RemoveIdentity(ctx, conn)
		if err != nil {
			return err
		}

		fmt.Printf("Identity deleted.\n")

		return nil
	} else {
		return attestation.OverwriteIdentity(ctx, conn, identity)
	}
}
