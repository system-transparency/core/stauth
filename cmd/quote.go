package cmd

import (
	"bytes"
	"context"
	"crypto/sha256"
	"crypto/subtle"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"golang.org/x/sys/unix"
	"google.golang.org/protobuf/proto"

	"git.glasklar.is/system-transparency/core/stauth/attestation"
	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/quotev0"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
	"git.glasklar.is/system-transparency/core/stauth/web"
)

func abs(p string) string {
	p, err := filepath.Abs(p)
	if err != nil {
		panic(err)
	}
	return p[1:]
}

var (
	quoteCmd = &cobra.Command{
		Use:   "quote",
		Short: "Quote command",
	}

	quoteHostCmd = &cobra.Command{
		Use:   "host",
		Short: "Host command",
		Run: func(cmd *cobra.Command, args []string) {
			ctx := cmd.Context()
			err := doQuoteHost(ctx, quoteListen)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				os.Exit(1)
			}
		},
	}

	quoteOperatorCmd = &cobra.Command{
		Use:   "operator [DEVICE-ADDRESS] [PLATFORM-ENDORSEMENT] [STBOOT-ENDORSEMENT] [OSPKG-ENDORSEMENT]",
		Short: "Operator command",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			var dataChannel *x509.Certificate
			var uxIdentity string

			ctx := cmd.Context()
			client := web.TofuClient(yesToAll)
			fsys := fs.FS(os.DirFS("/"))

			deviceUrl, err := url.Parse(args[0])
			if err == nil {
				dataChannel, uxIdentity, err = doQuoteOperator(ctx, &client, fsys, deviceUrl, abs(args[1]), abs(args[2]), abs(args[3]))
			}

			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				os.Exit(1)
			}

			fmt.Printf("%s validated\n", deviceUrl.Hostname())

			if dataChannel != nil {
				fmt.Printf("Data channel: %x\n", sha256.Sum256(dataChannel.Raw))
			} else {
				fmt.Printf("No data channel provisioned\n")
			}

			if uxIdentity != "" {
				fmt.Printf("UX identity: '%s'\n", uxIdentity)
			} else {
				fmt.Printf("No UX identity set\n")
			}
		},
	}

	quoteListen string
)

func init() {
	quoteHostCmd.Flags().StringVarP(&tpmDevicePath, "tpm", "t", "/dev/tpm0", "TPM device path")
	quoteHostCmd.Flags().StringVarP(&quoteListen, "listen", "l", "0.0.0.0:8080", "Listen address")

	quoteOperatorCmd.Flags().BoolVarP(&yesToAll, "yes", "y", false, "Answer yes to all questions")

	quoteCmd.AddCommand(quoteHostCmd, quoteOperatorCmd)
}

func newQuoteServer(ctx context.Context, fac connFactory, fsys fs.FS, mutex *sync.Mutex) http.Handler {
	log := zerolog.Ctx(ctx)

	r := chi.NewRouter()
	r.Post("/quotev0/request", func(w http.ResponseWriter, r *http.Request) {
		conn, err := fac()
		if err == nil {
			err = doQuoteRequest(log.WithContext(r.Context()), conn, mutex, w, fsys, r)
			conn.Close()
		}
		translateError(err, w)
	})

	return r
}

func doQuoteHost(ctx context.Context, listenAddress string) error {
	log := zerolog.Ctx(ctx)

	var mutex sync.Mutex

	tlsCfg, fpr, err := web.EphermalCertificate()
	if err != nil {
		return fmt.Errorf("error generating ephemeral certificate, %s", err)
	}

	log.Info().Msg("Opening TPM connection")

	conn, err := tpm2.OpenTPM(tpmDevicePath)
	for errors.Is(err, unix.EBUSY) {
		time.Sleep(1 * time.Second)
		conn, err = tpm2.OpenTPM(tpmDevicePath)
	}
	if err != nil {
		return fmt.Errorf("error opening TPM connection, %s", err)
	}

	err = tcg.FlushAllHandles(conn)
	if err != nil {
		return fmt.Errorf("error flushing handles, %s", err)
	}
	conn.Close()

	fsys := os.DirFS("/")
	factory := func() (io.ReadWriteCloser, error) {
		return tpm2.OpenTPM(tpmDevicePath)
	}

	r := newQuoteServer(ctx, factory, fsys, &mutex)

	srv := &http.Server{
		Addr:      listenAddress,
		Handler:   r,
		TLSConfig: tlsCfg,
	}

	fmt.Printf("Listening on %s\n", listenAddress)
	fmt.Println("Press Ctrl+C to exit")
	fmt.Printf("Certificate fingerprint: %s\n", fpr)
	fmt.Println("On the operator's machine, run:")
	fmt.Println("  stauth quote operator <DEVICE-ADDRESS> <PLATFORM-ENDORSEMENT> <STBOOT-ENDORSEMENT> <OSPKG-ENDORSEMENT>")
	if err := srv.ListenAndServeTLS("", ""); err != nil && err != http.ErrServerClosed {
		fmt.Printf("listen: %s\n", err)
	}
	fmt.Println("Server exiting")
	return nil
}

func doQuoteRequest(
	ctx context.Context,
	conn io.ReadWriteCloser,
	mutex *sync.Mutex,
	w http.ResponseWriter,
	fsys fs.FS,
	r *http.Request,
) error {
	log := zerolog.Ctx(ctx)

	// check content type
	if r.Header.Get("content-type") != quotev0.RequestContentType {
		log.Error().Msg("invalid content type")
		return errInvalidRequest
	}

	// read challenge
	chbuf, err := io.ReadAll(r.Body)
	if err != nil {
		log.Error().Err(err).Msg("error reading quote request")
		return errInternalError
	}

	// unmarshal challenge
	var req quotev0.Request
	err = proto.Unmarshal(chbuf, &req)
	if err != nil {
		log.Error().Err(err).Msg("error unmarshaling quote request")
		return errInternalError
	}

	mutex.Lock()
	resp, err := quotev0.NewResponse(conn, fsys, &req)
	mutex.Unlock()
	if err != nil {
		log.Error().Err(err).Msg("error creating quote response")
		return errInternalError
	}

	respbuf, err := proto.Marshal(resp)
	if err != nil {
		log.Error().Err(err).Msg("error marshaling quote response")
		return errInternalError
	}

	// send credential
	w.Header().Set("content-type", quotev0.ResponseContentType)
	_, err = w.Write(respbuf)
	if err != nil {
		log.Error().Err(err).Msg("error writing quote response")
		return errInternalError
	}

	return nil
}

func doQuoteOperator(
	ctx context.Context,
	client *http.Client,
	fsys fs.FS,
	deviceUrl *url.URL,
	platformPath string,
	stbootPath string,
	ospkgPath string,
) (*x509.Certificate, string, error) {
	log := zerolog.Ctx(ctx)

	// firmware measurements and AIK
	platformBuf, err := fs.ReadFile(fsys, platformPath)
	if err != nil {
		log.Error().Err(err).Msg("error reading platform measurements")
		return nil, "", errClient
	}
	platform, err := endorsev0.ParsePlatform(platformBuf)
	if err != nil {
		log.Error().Err(err).Msg("error parsing platform measurements")
		return nil, "", errClient
	}

	// compute the expected stboot measurements from the iso
	stbootBuf, err := fs.ReadFile(fsys, stbootPath)
	if err != nil {
		log.Error().Err(err).Msg("error reading stboot measurements")
		return nil, "", errClient
	}
	stboot, err := endorsev0.ParseStboot(stbootBuf)
	if err != nil {
		log.Error().Err(err).Msg("error parsing stboot measurements")
		return nil, "", errClient
	}

	// compute the expocted os-pkg measurements from the files
	ospkgBuf, err := fs.ReadFile(fsys, ospkgPath)
	if err != nil {
		log.Error().Err(err).Msg("error reading os-pkg measurements")
		return nil, "", errClient
	}
	ospkg, err := endorsev0.ParseOspkg(ospkgBuf)
	if err != nil {
		log.Error().Err(err).Msg("error parsing os-pkg measurements")
		return nil, "", errClient
	}

	// create a new quote request
	req, err := quotev0.NewRequest(platform.AikPublic, platform.AikPrivate, attestation.DefaultPCRList)
	if err != nil {
		log.Error().Err(err).Msg("error creating quote request")
		return nil, "", errClient
	}
	rqbuf, err := proto.Marshal(req)
	if err != nil {
		log.Error().Err(err).Msg("error marshaling quote request")
		return nil, "", errClient
	}

	deviceUrl.Path = "/quotev0/request"
	rqct := "application/protobuf; proto=quotev0.Request"
	rq, err := http.NewRequest(http.MethodPost, deviceUrl.String(), bytes.NewReader(rqbuf))
	if err != nil {
		log.Error().Err(err).Msg("error creating quote request")
		return nil, "", errClient
	}
	rq.Header.Set("Content-Type", rqct)

	resp, err := client.Do(rq)
	if err != nil {
		log.Error().Err(err).Msg("error requesting quote")
		return nil, "", errClient
	}
	defer resp.Body.Close()

	respbuf, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("error reading quote")
		return nil, "", errClient
	}

	if resp.StatusCode != http.StatusOK {
		log.Error().Err(err).Msg("error requesting quote")
		return nil, "", errClient
	}

	var q quotev0.Response
	err = proto.Unmarshal(respbuf, &q)
	if err != nil {
		log.Error().Err(err).Msg("error unmarshaling quote")
		return nil, "", errClient
	}

	// verify the quote
	aik, err := tpm2.DecodePublic(platform.AikPublic)
	if err != nil {
		log.Error().Err(err).Msg("error decoding AIK")
		return nil, "", errClient
	}
	attest, err := tpm2.DecodeAttestationData(q.Quote)
	if err != nil {
		log.Error().Err(err).Msg("error decoding quote")
		return nil, "", errClient
	}
	sig, err := tpm2.DecodeSignature(bytes.NewBuffer(q.Signature))
	if err != nil {
		log.Error().Err(err).Msg("error decoding quote signature")
		return nil, "", errClient
	}
	pcr := make([][]byte, len(q.Pcr))
	for i, p := range q.Pcr {
		pcr[i] = p.Value
	}
	err = tcg.Verify(*attest, *sig, pcr, aik)
	if err != nil {
		log.Error().Err(err).Msg("error verifying quote")
		return nil, "", errClient
	}
	if !bytes.Equal(attest.ExtraData, req.Nonce) {
		log.Error().Msg("quote nonce does not match request nonce")
		return nil, "", errClient
	}

	// make sure the quote is for the expected platform
	want, err := attestation.Reconstruct(attestation.DefaultPCRList, platform, stboot, ospkg)
	if err != nil {
		log.Error().Err(err).Msg("error reconstructing expected PCR values")
		return nil, "", errClient
	}
	goodQuote := true
	for _, p := range q.Pcr {
		if subtle.ConstantTimeCompare(p.Value, want[p.Index]) != 1 {
			log.Error().
				Int("pcr", int(p.Index)).
				Str("expected", fmt.Sprintf("%x", want[p.Index])).
				Str("received", fmt.Sprintf("%x", p.Value)).
				Msg("PCR values don't match")
			goodQuote = false
		}
	}
	if !goodQuote {
		log.Error().Msg("Quote does not match expected PCR values")
		err = attestation.AnalyseFailure(ctx, platform, stboot, ospkg, &q)
		if err != nil {
			log.Error().Err(err).Msg("error analysing failure")
		}
		return nil, "", errClient
	}

	var dataChannel *x509.Certificate
	if len(platform.GetDataChannel()) > 0 {
		dataChannel, err = x509.ParseCertificate(platform.GetDataChannel())
		if err != nil {
			log.Error().Err(err).Msg("error parsing data channel certificate")
			return nil, "", errClient
		}
	}

	return dataChannel, platform.UxIdentity, nil
}
