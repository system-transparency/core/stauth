package cmd

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"google.golang.org/protobuf/proto"

	"git.glasklar.is/system-transparency/core/stauth/attestation"
	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/web"
)

var (
	endorseCmd = &cobra.Command{
		Use: "endorse",
		Run: func(cmd *cobra.Command, args []string) {
			var err error

			ctx := cmd.Context()
			client := web.TofuClient(yesToAll)
			ospkgSet := cmd.Flags().Changed("ospkg-json") || cmd.Flags().Changed("ospkg-zip")
			stbootSet := cmd.Flags().Changed("stboot")
			platformHostSet := cmd.Flags().Changed("platform")
			platformSrvSet := cmd.Flags().Changed("platform-server")
			if ospkgSet && !stbootSet && !platformHostSet && !platformSrvSet {
				if ospkgZipPath != "" && ospkgJsonPath != "" {
					err = doEndorseOspkg(ospkgZipPath, ospkgJsonPath)
				} else {
					err = fmt.Errorf("both of --ospkg-zip and --ospkg-json must be set")
				}
			} else if !ospkgSet && stbootSet && !platformHostSet && !platformSrvSet {
				err = doEndorseStboot(stbootPath)
			} else if !ospkgSet && !stbootSet && platformHostSet && !platformSrvSet {
				var platformUrl *url.URL
				if !strings.HasPrefix(platformHost, "http://") && !strings.HasPrefix(platformHost, "https://") {
					platformHost = "https://" + platformHost
				}
				platformUrl, err = url.Parse(platformHost)
				if err == nil {
					err = doEnrollOperator(ctx, &client, platformUrl, true)
				}
			} else if !ospkgSet && !stbootSet && !platformHostSet && platformSrvSet {
				err = runEnrollmentServer(ctx, platformListen)
			} else {
				err = fmt.Errorf("exactly one of --ospkg-zip/json, --stboot, --platform-server, or --platform must be set")
			}

			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				os.Exit(1)
			}
		},
	}

	ospkgZipPath   string
	ospkgJsonPath  string
	stbootPath     string
	platformHost   string
	platformListen string

	outputPath    string
	tpmDevicePath string
	yesToAll      bool
)

func init() {
	// universal
	endorseCmd.Flags().StringVarP(&outputPath, "output", "o", "./", "Path to write the endorsement to")
	endorseCmd.Flags().StringVarP(&tpmDevicePath, "tpm", "t", "/dev/tpm0", "TPM device path")
	endorseCmd.Flags().BoolVarP(&yesToAll, "yes", "y", false, "Answer yes to all questions")

	// mutual exclusive
	endorseCmd.Flags().StringVar(&ospkgZipPath, "ospkg-zip", "", "Path to the OSPKG zip file")
	endorseCmd.Flags().StringVar(&ospkgJsonPath, "ospkg-json", "", "Path to the OSPKG json manifest")
	endorseCmd.Flags().StringVar(&stbootPath, "stboot", "", "Path to the stboot ISO")
	endorseCmd.Flags().StringVar(&platformHost, "platform", "", "Hostname of the platform")
	endorseCmd.Flags().StringVar(&platformListen, "platform-server", "", "Listen address of the platform")
}

func expandOutputPath(outputPath, nameHint, suffix string) (string, error) {
	myOutputPath := outputPath

	fi, err := os.Stat(myOutputPath)
	if err == nil {
		if fi.IsDir() {
			myOutputPath = path.Join(myOutputPath, fmt.Sprint(filepath.Base(nameHint), suffix))
		}
	} else if !os.IsNotExist(err) {
		return "", err
	}

	if err := os.MkdirAll(path.Dir(myOutputPath), 0755); err != nil {
		return "", err
	}
	return fmt.Sprint(strings.TrimSuffix(myOutputPath, suffix), suffix), nil
}

func doEndorseOspkg(ospkgZipPath, ospkgJsonPath string) error {
	// compute the expocted os-pkg measurements from the files
	ospkg, err := attestation.NewOsPkg(ospkgZipPath, ospkgJsonPath)
	if err != nil {
		return err
	}

	var outFile endorsev0.Endorsement
	outFile.Endorsement = &endorsev0.Endorsement_OsPkg{
		OsPkg: ospkg,
	}

	ospkgBuf, err := proto.Marshal(&outFile)
	if err != nil {
		return err
	}

	ospkgPath, err := expandOutputPath(outputPath, strings.TrimSuffix(path.Base(ospkgZipPath), ".zip"), ".ospkg.pb")
	if err != nil {
		return err
	}

	err = os.WriteFile(ospkgPath, ospkgBuf, 0644)
	if err != nil {
		return err
	}

	fmt.Printf("OS package endorsement written to %s\n", ospkgPath)
	return nil
}

func doEndorseStboot(stbootPath string) error {
	// compute the expoected stboot measurements from the iso
	stboot, err := attestation.NewStboot(stbootPath)
	if err != nil {
		return err
	}

	var outFile endorsev0.Endorsement
	outFile.Endorsement = &endorsev0.Endorsement_Stboot{
		Stboot: stboot,
	}

	stbootBuf, err := proto.Marshal(&outFile)
	if err != nil {
		return err
	}

	stbootPath, err = expandOutputPath(outputPath, strings.TrimSuffix(path.Base(stbootPath), ".iso"), ".stboot.pb")
	if err != nil {
		return err
	}

	err = os.WriteFile(stbootPath, stbootBuf, 0644)
	if err != nil {
		return err
	}

	fmt.Printf("Stboot endorsement written to %s\n", stbootPath)
	return nil
}
