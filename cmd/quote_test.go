package cmd

import (
	"context"
	"crypto/sha256"
	"io"
	"net/http/httptest"
	"net/url"
	"os"
	"sync"
	"testing"
	"testing/fstest"
	"time"

	"github.com/google/go-tpm-tools/simulator"
	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/google/go-tpm/tpmutil"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"google.golang.org/protobuf/proto"

	"git.glasklar.is/system-transparency/core/stauth/attestation"
	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/metadata"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
)

func TestQuote(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests in short mode.")
	}

	// logger
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	log := zerolog.New(output).With().Timestamp().Logger()
	ctx := log.WithContext(context.Background())

	// mock FS
	var stboot, ospkg endorsev0.Endorsement
	mockFs := make(fstest.MapFS)
	stbootbuf, err := os.ReadFile("../testdata/stboot.stboot.pb")
	assert.NoError(t, err)
	err = proto.Unmarshal(stbootbuf, &stboot)
	assert.NoError(t, err)
	ospkgbuf, err := os.ReadFile("../testdata/os-pkg-example-ubuntu20.ospkg.pb")
	assert.NoError(t, err)
	err = proto.Unmarshal(ospkgbuf, &ospkg)
	assert.NoError(t, err)
	metabuf, err := os.ReadFile("../testdata/metadata-nontxt.binary")
	assert.NoError(t, err)
	mockFs["stboot"] = &fstest.MapFile{Data: stbootbuf, Mode: 0644}
	mockFs["ospkg"] = &fstest.MapFile{Data: ospkgbuf, Mode: 0644}
	mockFs["dev/pmem0"] = &fstest.MapFile{Data: metabuf, Mode: 0644}

	sim, err := simulator.Get()
	assert.NoError(t, err)
	defer sim.Close()
	err = attestation.OverwriteIdentity(ctx, sim, "test.example.com")
	assert.NoError(t, err)

	// build mock platform endorsement
	srk, srkbuf, aik, aikbuf, aikpriv, err := tcg.GenerateKeys(sim)
	assert.NoError(t, err)
	tcg.FlushKeys(sim, srk, aik)

	srkpub, err := tpm2.DecodePublic(srkbuf)
	assert.NoError(t, err)
	aikpub, err := tpm2.DecodePublic(aikbuf)
	assert.NoError(t, err)

	uefilogbuf, err := os.ReadFile("../testdata/log-nontxt.binary")
	assert.NoError(t, err)
	stbootlog, err := metadata.Read(metadata.EventLog, mockFs)
	assert.NoError(t, err)

	plt, err := attestation.NewPlatform(uefilogbuf, stbootlog, srkpub, aikpub, aikpriv, "test.example.com", nil)
	assert.NoError(t, err)

	var pltfile endorsev0.Endorsement
	pltfile.Endorsement = &endorsev0.Endorsement_Platform{
		Platform: plt,
	}
	pltbuf, err := proto.Marshal(&pltfile)
	assert.NoError(t, err)
	mockFs["plt"] = &fstest.MapFile{Data: pltbuf, Mode: 0644}

	// measure expected values into TPM simulator
	for _, ev := range plt.GetLog() {
		digest := ev.GetDigest()
		pcr := tpmutil.Handle(int32(tpm2.PCRFirst) + ev.GetPcr())

		switch ev.GetType() {
		case endorsev0.Platform_Entry_INIT:
			continue

		case endorsev0.Platform_Entry_OPAQUE:
			// nop

		case endorsev0.Platform_Entry_STBOOT_UKI:
			digest = stboot.GetStboot().GetUki()

		case endorsev0.Platform_Entry_STBOOT_LINUX:
			digest = stboot.GetStboot().GetLinux()

		case endorsev0.Platform_Entry_STBOOT_INITRD:
			digest = stboot.GetStboot().GetInitrd()

		case endorsev0.Platform_Entry_STBOOT_CMDLINE:
			digest = stboot.GetStboot().GetCmdline()

		case endorsev0.Platform_Entry_STBOOT_OSREL:
			digest = stboot.GetStboot().GetOsrel()

		case endorsev0.Platform_Entry_STBOOT_AUTHENTIHASH:
			digest = stboot.GetStboot().GetAuthentihash()

		case endorsev0.Platform_Entry_OSPKG_ZIP:
			sum := sha256.Sum256(ospkg.GetOsPkg().GetZip())
			digest = sum[:]

		case endorsev0.Platform_Entry_OSPKG_DESCRIPTOR:
			sum := sha256.Sum256(ospkg.GetOsPkg().GetDescriptor_())
			digest = sum[:]

		case endorsev0.Platform_Entry_OSPKG_SECURITYCONFIG:
			sum := sha256.Sum256(stboot.GetStboot().GetSecurityConfig())
			digest = sum[:]

		case endorsev0.Platform_Entry_OSPKG_SIGNINGROOT:
			sum := sha256.Sum256(stboot.GetStboot().GetSigningRoot())
			digest = sum[:]

		case endorsev0.Platform_Entry_OSPKG_HTTPSROOTS:
			for _, d := range stboot.GetStboot().GetHttpsRoots() {
				sum := sha256.Sum256(d)
				err = tpm2.PCRExtend(sim, pcr, tpm2.AlgSHA256, tpmutil.U16Bytes(sum[:]), "")
				assert.NoError(t, err)
			}
			continue
		case endorsev0.Platform_Entry_UX_IDENTITY:
			sum := sha256.Sum256([]byte(plt.UxIdentity))
			digest = sum[:]
		case endorsev0.Platform_Entry_DATA_CHANNEL:
			sum := sha256.Sum256(plt.DataChannel)
			digest = sum[:]

		default:
			assert.Fail(t, "unknown event type")
		}

		err = tpm2.PCRExtend(sim, pcr, tpm2.AlgSHA256, tpmutil.U16Bytes(digest), "")
		assert.NoError(t, err)
	}

	factory := func() (io.ReadWriteCloser, error) {
		return sim, nil
	}

	var mutex sync.Mutex
	r := newQuoteServer(ctx, factory, mockFs, &mutex)
	assert.NoError(t, err)

	srv := httptest.NewServer(r)
	defer srv.Close()
	client := srv.Client()

	u, err := url.Parse(srv.URL)
	assert.NoError(t, err)

	_, _, err = doQuoteOperator(ctx, client, mockFs, u, "plt", "stboot", "ospkg")
	assert.NoError(t, err)
}
