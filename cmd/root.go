package cmd

import (
	"fmt"
	"os"
	"runtime/debug"
	"strings"

	"github.com/spf13/cobra"
	"google.golang.org/protobuf/proto"

	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
)

var (
	// version info
	commit    = "(unknown)"
	protocols = []string{"endorsev0", "enrollv0", "quotev0"}

	RootCmd = &cobra.Command{
		Use:   "stauth",
		Short: "stauth enrolling and registry tool",
	}
	printCmd = &cobra.Command{
		Use:   "print [file...]",
		Short: "Print the contents of a stauth state file *.stauth.pb",
		Run:   doPrint,
	}
	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Print the version number of stauth",
		Run:   doVersion,
	}
)

func init() {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				commit = setting.Value
			}
		}
	}
	RootCmd.AddCommand(endorseCmd, quoteCmd, identityCmd, printCmd, versionCmd)
}

func doVersion(cmd *cobra.Command, args []string) {
	fmt.Printf(`stauth version %s
Implemented protocols: %s
`, commit, strings.Join(protocols, ", "))
}

func doPrint(cmd *cobra.Command, args []string) {
	for _, file := range args {
		buf, err := os.ReadFile(file)
		if err != nil {
			fmt.Printf("error opening %s: %v\n", file, err)
			continue
		}

		var state endorsev0.Endorsement

		err = proto.Unmarshal(buf, &state)
		if err != nil {
			fmt.Printf("error unmarshalling %s: %v\n", file, err)
			continue
		}

		fmt.Printf("File: %s\n", file)
		switch state.Endorsement.(type) {
		case *endorsev0.Endorsement_Unspecified:
			fmt.Printf("Unspecified file contents\n")

		case *endorsev0.Endorsement_Platform:
			fmt.Print(state.GetPlatform().Display())

		case *endorsev0.Endorsement_Stboot:
			fmt.Printf("Stboot: %s\n", state.GetStboot().String())

		case *endorsev0.Endorsement_OsPkg:
			fmt.Printf("OsPkg: %s\n", state.GetOsPkg().String())

		default:
			fmt.Printf("Unknown file contents\n")
		}
	}
}
