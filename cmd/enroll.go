package cmd

import (
	"bytes"
	"context"
	"crypto/subtle"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/rs/zerolog"
	"google.golang.org/protobuf/proto"

	"git.glasklar.is/system-transparency/core/stauth/attestation"
	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/enrollv0"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
	"git.glasklar.is/system-transparency/core/stauth/web"
)

type connFactory func() (io.ReadWriteCloser, error)

var (
	errInternalError  = errors.New("internal server error")
	errInvalidRequest = errors.New("invalid request")

	errClient = errors.New("client error")
	errServer = errors.New("server error")
	errFormat = errors.New("format error")

	stripPortRegexp = regexp.MustCompile(`:[0-9]+$`)
)

func doEnrollHostRequest(
	ctx context.Context,
	conn io.ReadWriteCloser,
	mutex *sync.Mutex,
	ekcert *x509.Certificate,
	identity string,
	w http.ResponseWriter,
	r *http.Request,
) error {
	log := zerolog.Ctx(ctx)

	// create SRK and AIK
	mutex.Lock()
	req, err := enrollv0.NewRequest(conn, ekcert, identity)
	mutex.Unlock()

	if err != nil {
		log.Error().Err(err).Msg("error creating request")
		return errInternalError
	}

	// marshal request
	reqbuf, err := proto.Marshal(req)
	if err != nil {
		log.Error().Err(err).Msg("error marshaling request")
		return errInternalError
	}

	// send request
	_, err = w.Write(reqbuf)
	if err != nil {
		log.Error().Err(err).Msg("error sending request")
		return errInternalError
	}

	return nil
}

func doEnrollHostChallenge(
	ctx context.Context,
	conn io.ReadWriteCloser,
	mutex *sync.Mutex,
	fsys fs.FS,
	w http.ResponseWriter,
	r *http.Request,
) error {
	log := zerolog.Ctx(ctx)

	// check content type
	if r.Header.Get("content-type") != enrollv0.ChallengeContentType {
		log.Error().Msg("invalid content type")
		return errInvalidRequest
	}

	// read challenge
	chbuf, err := io.ReadAll(r.Body)
	if err != nil {
		log.Error().Err(err).Msg("error reading challenge")
		return errInternalError
	}

	// unmarshal challenge
	var ch enrollv0.Challenge
	err = proto.Unmarshal(chbuf, &ch)
	if err != nil {
		log.Error().Err(err).Msg("error unmarshaling challenge")
		return errInternalError
	}

	// get event logs
	uefilog, stbootlog, err := tcg.ReadEventLogs(fsys)
	if err != nil {
		log.Error().Err(err).Msg("error reading UEFI event log")
		return err
	}
	if len(stbootlog) == 0 {
		log.Warn().Err(err).Msg("error reading stboot event log")
	}

	// get data channel identity
	dataChannel, err := attestation.DataChannel(fsys)
	if err != nil {
		log.Warn().Err(err).Msg("error reading data channel identity")
	}

	// activate AIK credential
	mutex.Lock()
	answer, err := enrollv0.NewAnswer(conn, &ch, uefilog, stbootlog, dataChannel)
	mutex.Unlock()

	if err != nil {
		log.Error().Err(err).Msg("error creating answer")
		return errInternalError
	}

	// marshal credential
	ansbuf, err := proto.Marshal(answer)
	if err != nil {
		log.Error().Err(err).Msg("error marshaling answer")
		return errInternalError
	}

	// send credential
	w.Header().Set("content-type", enrollv0.AnswerContentType)
	_, err = w.Write(ansbuf)
	if err != nil {
		log.Error().Err(err).Msg("error sending answer")
		return errInternalError
	}

	return nil
}

func translateError(err error, w http.ResponseWriter) {
	switch err {
	default:
		fallthrough
	case errInternalError:
		http.Error(w, "internal server error", http.StatusInternalServerError)

	case errInvalidRequest:
		http.Error(w, "invalid request", http.StatusBadRequest)

	case nil:
	}
}

func newEnrollmentServer(ctx context.Context, fac connFactory, fsys fs.FS) (http.Handler, error) {
	log := zerolog.Ctx(ctx)
	mutex := sync.Mutex{}

	conn, err := fac()
	if err != nil {
		log.Error().Err(err).Msg("error connecting to TPM")
		return nil, err
	}
	defer conn.Close()

	// get TPM info
	info, err := tcg.Properties(conn)
	if err != nil {
		log.Error().Err(err).Msg("error reading TPM info")
		return nil, err
	}
	fmt.Printf("Found %s TPM %s \"%s\", implementing spec level %d rev %d.%d released %s\n",
		info.Manufacturer, info.Family, info.VendorString, info.Level, info.RevisionMajor,
		info.RevisionMinor, info.ReleaseDate.Format("2006-01-02"))

	// get EK certificate
	ekcert, err := tcg.EndorsementCertificate(conn)
	if err == tcg.ErrNoSuchHandle {
		ekcert = nil
		fmt.Printf("No EK certificate found\n")
	} else if err != nil {
		ekcert = nil
		fmt.Printf("Error reading EK certificate, %s\n", err)
	} else {
		fmt.Printf("Endorsed by %s\n", ekcert.Issuer.CommonName)
	}

	// get UX identity
	id, err := attestation.Identity(ctx, conn)
	if err != nil {
		log.Warn().Err(err).Msg("error reading UX identity")
		id = ""

		fmt.Printf("No UX identity provisioned. Use `./stauth identity <IDENTITY>` to set it.\n")
	} else {
		fmt.Printf("UX identity: %s\n", id)
	}

	// clear all handles
	err = tcg.FlushAllHandles(conn)
	if err != nil {
		log.Error().Err(err).Msg("error flushing handles")
		return nil, err
	}

	conn.Close()

	r := chi.NewRouter()
	r.Get("/enrollv0/request", func(w http.ResponseWriter, r *http.Request) {
		conn, err := fac()
		if err == nil {
			err = doEnrollHostRequest(log.WithContext(r.Context()), conn, &mutex, ekcert, id, w, r)
			conn.Close()
		}
		translateError(err, w)
	})
	r.Post("/enrollv0/activate", func(w http.ResponseWriter, r *http.Request) {
		conn, err := fac()
		if err == nil {
			err = doEnrollHostChallenge(log.WithContext(r.Context()), conn, &mutex, fsys, w, r)
			conn.Close()
		}
		translateError(err, w)
	})

	return r, nil
}

func runEnrollmentServer(ctx context.Context, listenAddress string) error {
	// issue a fresh TLS certificate
	tlsCfg, fpr, err := web.EphermalCertificate()
	if err != nil {
		fmt.Printf("Error creating ephemeral certificate: %s\n", err)
		return err
	}

	factory := func() (io.ReadWriteCloser, error) {
		return tpm2.OpenTPM(tpmDevicePath)
	}
	fsys := os.DirFS("/")

	r, err := newEnrollmentServer(ctx, factory, fsys)
	if err != nil {
		fmt.Printf("Error creating enrollment server: %s\n", err)
		return err
	}

	myctx, cancel := context.WithCancel(ctx)
	srv := &http.Server{
		Addr:      listenAddress,
		Handler:   r,
		TLSConfig: tlsCfg,
	}
	srv.RegisterOnShutdown(func() { cancel() })

	host, err := os.Hostname()
	if err != nil {
		host = "<THIS HOST>"
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()

		fmt.Printf("Listening on %s\n", listenAddress)
		fmt.Println("Press Ctrl+C to exit")
		fmt.Printf("Certificate fingerprint: %s\n", fpr)
		fmt.Println("On the operator's machine, run:")
		fmt.Printf("  stauth endorse --platform %s:8080\n", host)
		if err := srv.ListenAndServeTLS("", ""); err != nil && err != http.ErrServerClosed {
			fmt.Printf("listen: %s\n", err)
		}
	}()

	<-myctx.Done()
	fmt.Println("Shutting down server")
	err = srv.Shutdown(ctx)
	wg.Wait()

	fmt.Println("Server exiting")
	return err
}

func doEnrollOperatorRequest(ctx context.Context, client *http.Client, deviceUrl *url.URL) (*enrollv0.Request, error) {
	log := zerolog.Ctx(ctx)

	deviceUrl.Path = "/enrollv0/request"
	rq, err := http.NewRequest(http.MethodGet, deviceUrl.String(), nil)
	if err != nil {
		log.Error().Err(err).Msg("error creating request")
		return nil, errInternalError
	}
	resp, err := client.Do(rq)
	if err != nil {
		log.Error().Err(err).Msg("error requesting enrollment")
		return nil, errServer
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Error().Int("status", resp.StatusCode).Msg("error requesting enrollment")
		return nil, errServer
	}

	reqbuf, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("error reading request")
		return nil, errServer
	}

	var req enrollv0.Request
	err = proto.Unmarshal(reqbuf, &req)
	if err != nil {
		log.Error().Err(err).Msg("error unmarshaling request")
		return nil, errFormat
	}

	return &req, nil
}

func doEnrollOperatorChallenge(ctx context.Context, client *http.Client, deviceUrl *url.URL, nonce []byte, ch *enrollv0.Challenge) (*enrollv0.Answer, error) {
	log := zerolog.Ctx(ctx)

	chbuf, err := proto.Marshal(ch)
	if err != nil {
		log.Error().Err(err).Msg("error marshaling challenge")
		return nil, errClient
	}

	deviceUrl.Path = "/enrollv0/activate"
	rq, err := http.NewRequestWithContext(ctx, http.MethodPost, deviceUrl.String(), bytes.NewReader(chbuf))
	if err != nil {
		log.Error().Err(err).Msg("error creating challenge request")
		return nil, errInternalError
	}
	rq.Header.Set("Content-Type", enrollv0.ChallengeContentType)
	resp, err := client.Do(rq)
	if err != nil {
		log.Error().Err(err).Msg("error activating enrollment")
		return nil, errServer
	}
	defer resp.Body.Close()

	ansbuf, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("error reading answer")
		return nil, errServer
	}

	if resp.StatusCode != http.StatusOK {
		log.Error().Int("status", resp.StatusCode).Msg("error activating enrollment")
		return nil, errServer
	}

	// unmarshal and verify challenge answer
	var ans enrollv0.Answer
	err = proto.Unmarshal(ansbuf, &ans)
	if err != nil {
		log.Error().Err(err).Msg("error unmarshaling answer")
		return nil, errFormat
	}

	if subtle.ConstantTimeCompare(ans.Nonce, nonce) != 1 {
		log.Error().Msg("nonce mismatch")
		return nil, errServer
	}

	return &ans, nil
}

func doEnrollOperatorFinish(
	ctx context.Context,
	nameHint string,
	req *enrollv0.Request,
	ans *enrollv0.Answer,
	writeFile bool,
) (string, error) {
	enrollOutputPath := outputPath
	log := zerolog.Ctx(ctx)

	fi, err := os.Stat(enrollOutputPath)
	if err == nil {
		if fi.IsDir() {
			enrollOutputPath = path.Join(enrollOutputPath, fmt.Sprint(filepath.Base(nameHint), ".platform.pb"))
		}
	} else if !os.IsNotExist(err) {
		log.Error().Err(err).Msg("error checking output path")
		return "", errClient
	}

	if err := os.MkdirAll(path.Dir(enrollOutputPath), 0755); err != nil {
		log.Error().Err(err).Msg("error creating output directory")
		return "", errClient
	}
	outputPath := fmt.Sprint(strings.TrimSuffix(enrollOutputPath, ".platform.pb"), ".platform.pb")

	srk, err := tpm2.DecodePublic(req.SrkPublic)
	if err != nil {
		log.Error().Err(err).Msg("error decoding SRK public")
		return "", errClient
	}

	aik, err := tpm2.DecodePublic(req.AikPublic)
	if err != nil {
		log.Error().Err(err).Msg("error decoding AIK public")
		return "", errClient
	}

	dataChannel, err := x509.ParseCertificate(ans.DataChannelIdentity)
	if err != nil {
		if len(ans.DataChannelIdentity) > 0 {
			log.Error().Err(err).Msg("error parsing data channel certificate")
			return "", errClient
		} else {
			log.Warn().Err(err).Msg("error parsing data channel certificate")
		}
	}

	platformData, err := attestation.NewPlatform(ans.UefiEventLog, ans.StbootEventLog, srk, aik, req.AikPrivate, req.UxIdentity, dataChannel)
	if err != nil {
		log.Error().Err(err).Msg("error creating platform data")
		return "", errClient
	}

	var outFile endorsev0.Endorsement
	outFile.Endorsement = &endorsev0.Endorsement_Platform{
		Platform: platformData,
	}

	outbuf, err := proto.Marshal(&outFile)
	if err != nil {
		log.Error().Err(err).Msg("error marshaling platform data")
		return "", errClient
	}

	if writeFile {
		err = os.WriteFile(outputPath, outbuf, 0644)
		if err != nil {
			log.Error().Err(err).Msg("error writing platform data")
			return "", errClient
		}
	}

	return outputPath, nil
}

func doEnrollOperator(ctx context.Context, client *http.Client, deviceUrl *url.URL, writeFile bool) error {
	log := zerolog.Ctx(ctx)

	// get AIK, EK, and SRK
	req, err := doEnrollOperatorRequest(ctx, client, deviceUrl)
	if err != nil {
		return err
	}

	// send AIK credential encrypted with EK
	ch, nonce, err := enrollv0.NewChallenge(req)
	if err != nil {
		log.Error().Err(err).Msg("error creating challenge")
		return errClient
	}
	ans, err := doEnrollOperatorChallenge(ctx, client, deviceUrl, nonce, ch)
	if err != nil {
		return err
	}

	// write out platform data
	nameHint := req.UxIdentity
	if nameHint == "" {
		nameHint = stripPortRegexp.ReplaceAllString(deviceUrl.Host, "")
	}

	outputPath, err := doEnrollOperatorFinish(ctx, nameHint, req, ans, writeFile)
	if err != nil {
		return err
	}

	fmt.Printf("Plaform data written to %s\n", outputPath)
	return nil
}
