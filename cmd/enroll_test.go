package cmd

import (
	"context"
	"io"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
	"testing/fstest"
	"time"

	"github.com/google/go-tpm-tools/simulator"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"

	"git.glasklar.is/system-transparency/core/stauth/attestation"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
)

type fakeCloser struct {
	inner io.ReadWriteCloser
}

func newFakeCloser(inner io.ReadWriteCloser) *fakeCloser {
	return &fakeCloser{inner: inner}
}

func (f *fakeCloser) Read(p []byte) (n int, err error) {
	return f.inner.Read(p)
}

func (f *fakeCloser) Write(p []byte) (n int, err error) {
	return f.inner.Write(p)
}

func (f *fakeCloser) Close() error {
	return nil
}

func TestEnroll(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests in short mode.")
	}

	// logger
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	log := zerolog.New(output).With().Timestamp().Logger()
	ctx := log.WithContext(context.Background())

	// mock FS
	mockFs := make(fstest.MapFS)
	uefilog, err := os.ReadFile("../testdata/log-nontxt.binary")
	assert.NoError(t, err)
	meta, err := os.ReadFile("../testdata/metadata-nontxt.binary")
	assert.NoError(t, err)
	mockFs[tcg.UefiEventLogPath] = &fstest.MapFile{Data: uefilog, Mode: 0644}
	mockFs["dev/pmem0"] = &fstest.MapFile{Data: meta, Mode: 0644}

	sim, err := simulator.Get()
	assert.NoError(t, err)
	defer sim.Close()
	err = attestation.OverwriteIdentity(ctx, sim, "test.example.com")
	assert.NoError(t, err)

	factory := func() (io.ReadWriteCloser, error) {
		return newFakeCloser(sim), nil
	}

	r, err := newEnrollmentServer(ctx, factory, mockFs)
	assert.NoError(t, err)

	srv := httptest.NewServer(r)
	defer srv.Close()
	client := srv.Client()

	u, err := url.Parse(srv.URL)
	assert.NoError(t, err)
	err = doEnrollOperator(ctx, client, u, false)
	assert.NoError(t, err)
}
