package endorsev0

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"fmt"

	"github.com/google/go-tpm/legacy/tpm2"
	"google.golang.org/protobuf/proto"
)

//go:generate protoc --go_opt=module=git.glasklar.is/core/stauth/endorsev0 --go_out=./ endorsev0.proto

func ParsePlatform(buf []byte) (*Platform, error) {
	var state Endorsement

	err := proto.Unmarshal(buf, &state)
	if err != nil {
		return nil, err
	}

	switch state.Endorsement.(type) {
	case *Endorsement_Platform:
		return state.GetPlatform(), nil
	default:
		return nil, fmt.Errorf("unexpected state type: %T", state.Endorsement)
	}
}

func ParseStboot(buf []byte) (*Stboot, error) {
	var state Endorsement

	err := proto.Unmarshal(buf, &state)
	if err != nil {
		return nil, err
	}

	switch state.Endorsement.(type) {
	case *Endorsement_Stboot:
		return state.GetStboot(), nil
	default:
		return nil, fmt.Errorf("unexpected state type: %T", state.Endorsement)
	}
}

func ParseOspkg(buf []byte) (*OsPkg, error) {
	var state Endorsement

	err := proto.Unmarshal(buf, &state)
	if err != nil {
		return nil, err
	}

	switch state.Endorsement.(type) {
	case *Endorsement_OsPkg:
		return state.GetOsPkg(), nil
	default:
		return nil, fmt.Errorf("unexpected state type: %T", state.Endorsement)
	}
}

func (p *Platform) PublicKey() (crypto.PublicKey, error) {
	key, err := tpm2.DecodePublic(p.AikPublic)
	if err != nil {
		return nil, err
	}
	return key.Key()
}

func (p *Platform) Name() (*tpm2.Name, error) {
	return tpm2.DecodeName(bytes.NewBuffer(p.AikQname))
}

func (p *Platform) NameAlgorithm() (crypto.Hash, error) {
	name, err := p.Name()
	if err != nil {
		return 0, err
	}
	return name.Digest.Alg.Hash()
}

func (p *Platform) Display() string {
	out := new(bytes.Buffer)

	out.WriteString("Platform:\n")
	out.WriteString("  AIK Qualified Name: ")
	name, err := p.Name()
	if err != nil {
		out.WriteString("ERROR: ")
		out.WriteString(err.Error())
	} else {
		fmt.Fprintf(out, "%x", name.Digest.Value)
	}
	out.WriteString("\n")
	out.WriteString("  AIK Type: ")
	key, err := p.PublicKey()
	if err != nil {
		out.WriteString("ERROR: ")
		out.WriteString(err.Error())
	} else {
		switch key := key.(type) {
		case *ecdsa.PublicKey:
			fmt.Fprintf(out, "ECDSA over %s", key.Params().Name)
		case *rsa.PublicKey:
			fmt.Fprintf(out, "RSA %d", key.Size()*8)
		default:
			fmt.Fprintf(out, "UNKNOWN: %T", key)
		}
	}
	out.WriteString("\n")
	out.WriteString("  UX Identity: ")
	if p.UxIdentity == "" {
		out.WriteString("(none)")
	} else {
		out.WriteString(p.UxIdentity)
	}
	out.WriteString("\n")
	out.WriteString("  Data Channel Identity: ")
	if p.DataChannel == nil {
		out.WriteString("(none)")
	} else {
		cert, err := x509.ParseCertificate(p.DataChannel)
		if err != nil {
			out.WriteString("ERROR: ")
			out.WriteString(err.Error())
		} else if cert.Subject.CommonName == "" {
			fmt.Fprintf(out, "%x", sha256.Sum256(cert.Raw))
		} else {
			fmt.Fprintf(out, "%s", cert.Subject.CommonName)
		}
	}
	out.WriteString("\n")
	fmt.Fprintf(out, "  Log Template: %d entries\n", len(p.Log))
	for _, entry := range p.Log {
		fmt.Fprintf(out, "    PCR[%02d] %s %x\n", entry.Pcr, Platform_Entry_Type_name[int32(entry.Type)], entry.Digest)
	}
	out.WriteString("\n")

	return out.String()
}
