package web

import (
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

type verifyConnectionFunction = func(raw [][]byte, verifiedChains [][]*x509.Certificate) error

var (
	errCertNotAccepted            = errors.New("certificate not accepted")
	errCertChainNotExpected       = errors.New("certificate chain not expected")
	errMultipleServerCertificates = errors.New("multiple server certificates")

	packExpr = regexp.MustCompile(`[a-fA-F0-9]{4}`)
)

func verifyConnection(yesToAll bool) verifyConnectionFunction {
	return func(raw [][]byte, verifiedChains [][]*x509.Certificate) error {
		if len(verifiedChains) != 0 {
			return errCertChainNotExpected
		}
		if len(raw) != 1 {
			return errMultipleServerCertificates
		}
		if yesToAll {
			return nil
		}
		sum := sha256.Sum256(raw[0])
		words := packExpr.FindAllString(fmt.Sprintf("%x", sum[:]), -1)
		fmt.Printf("Accept server certificate with SHA256 fingerprint %s? [y/N] ", strings.Join(words, " "))
		var answer string
		_, err := fmt.Scanln(&answer)
		if err != nil {
			return errCertNotAccepted
		}
		if strings.ToLower(answer) != "y" {
			return errCertNotAccepted
		}
		return nil
	}
}

func TofuClient(yesToAll bool) http.Client {
	return http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				VerifyPeerCertificate: verifyConnection(yesToAll),
				InsecureSkipVerify:    true,
			},
		},
	}
}
