package tcg

import (
	"bytes"
	"crypto/x509"
	"encoding/binary"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/google/go-tpm/tpmutil"
)

var (
	DefaultSRKTemplate = tpm2.Public{
		Type:    tpm2.AlgECC,
		NameAlg: tpm2.AlgSHA256,
		Attributes: tpm2.FlagFixedTPM |
			tpm2.FlagFixedParent |
			tpm2.FlagSensitiveDataOrigin |
			tpm2.FlagUserWithAuth |
			tpm2.FlagNoDA |
			tpm2.FlagRestricted |
			tpm2.FlagDecrypt,
		ECCParameters: &tpm2.ECCParams{
			Symmetric: &tpm2.SymScheme{
				Alg:     tpm2.AlgAES,
				KeyBits: 128,
				Mode:    tpm2.AlgCFB,
			},
			CurveID: tpm2.CurveNISTP256,
		},
	}
	DefaultAIKTemplate = tpm2.Public{
		Type:    tpm2.AlgECC,
		NameAlg: tpm2.AlgSHA256,
		Attributes: tpm2.FlagFixedTPM |
			tpm2.FlagFixedParent |
			tpm2.FlagSensitiveDataOrigin |
			tpm2.FlagUserWithAuth |
			tpm2.FlagRestricted |
			tpm2.FlagSign,
		ECCParameters: &tpm2.ECCParams{
			Sign: &tpm2.SigScheme{
				Alg:   tpm2.AlgECDSA,
				Hash:  tpm2.AlgSHA256,
				Count: 0,
			},
			CurveID: tpm2.CurveNISTP256,
		},
	}
)

type TPMProperties struct {
	Manufacturer    string
	Family          string
	Level           int
	RevisionMajor   int
	RevisionMinor   int
	ReleaseDate     time.Time
	FirmwareVersion uint64
	VendorString    string
}

func Properties(conn io.ReadWriteCloser) (*TPMProperties, error) {
	manu, err := tpm2.GetManufacturer(conn)
	if err != nil {
		return nil, err
	}

	spec, _, err := tpm2.GetCapability(conn, tpm2.CapabilityTPMProperties, 5, uint32(tpm2.FamilyIndicator))
	if err != nil {
		return nil, err
	}
	if len(spec) != 5 {
		return nil, fmt.Errorf("unexpected number of properties returned")
	}

	specString := new(bytes.Buffer)
	err = binary.Write(specString, binary.BigEndian, spec[0].(tpm2.TaggedProperty).Value)
	if err != nil {
		return nil, err
	}

	releaseDate := time.Date(int(spec[4].(tpm2.TaggedProperty).Value), time.January, 1, 0, 0, 0, 0, time.UTC)
	releaseDate = releaseDate.AddDate(0, 0, int(spec[3].(tpm2.TaggedProperty).Value))

	ver, _, err := tpm2.GetCapability(conn, tpm2.CapabilityTPMProperties, 2, uint32(tpm2.FirmwareVersion1))
	if err != nil {
		return nil, err
	}
	if len(ver) != 2 {
		return nil, fmt.Errorf("unexpected number of properties returned")
	}

	model, _, err := tpm2.GetCapability(conn, tpm2.CapabilityTPMProperties, 4, uint32(tpm2.VendorString1))
	if err != nil {
		return nil, err
	}
	if len(model) != 4 {
		return nil, fmt.Errorf("unexpected number of properties returned")
	}

	vendorString := new(bytes.Buffer)
	for _, v := range model {
		err := binary.Write(vendorString, binary.BigEndian, v.(tpm2.TaggedProperty).Value)
		if err != nil {
			return nil, err
		}
	}

	return &TPMProperties{
		Manufacturer:    string(manu),
		Family:          strings.Trim(specString.String(), "\x00"),
		Level:           int(spec[1].(tpm2.TaggedProperty).Value),
		RevisionMajor:   int(spec[2].(tpm2.TaggedProperty).Value) / 100,
		RevisionMinor:   int(spec[2].(tpm2.TaggedProperty).Value) % 100,
		ReleaseDate:     releaseDate,
		FirmwareVersion: (uint64(ver[0].(tpm2.TaggedProperty).Value) << 32) | uint64(ver[1].(tpm2.TaggedProperty).Value),
		VendorString:    vendorString.String(),
	}, nil
}

func EndorsementCertificate(conn io.ReadWriteCloser) (*x509.Certificate, error) {
	err := NvHandle(conn, defaultEKCertificateIndex)
	if err != nil {
		return nil, err
	}
	certbuf, err := tpm2.NVRead(conn, defaultEKCertificateIndex)
	if err != nil {
		return nil, err
	}

	return x509.ParseCertificate(certbuf)
}

func LoadKeys(conn io.ReadWriteCloser, aikpub []byte, aikpriv []byte) (tpmutil.Handle, tpmutil.Handle, error) {
	var sel tpm2.PCRSelection

	// Generate SRK
	srk, _, _, _, _, _, err := tpm2.CreatePrimaryEx(conn, tpm2.HandleOwner, sel, "", "", DefaultSRKTemplate)
	if err != nil {
		return 0, 0, err
	}

	// Load AIK
	aik, _, err := tpm2.Load(conn, srk, "", aikpub, aikpriv)
	if err != nil {
		_ = tpm2.FlushContext(conn, srk)
		return 0, 0, err
	}

	return srk, aik, nil
}

func GenerateKeys(conn io.ReadWriteCloser) (tpmutil.Handle, []byte, tpmutil.Handle, []byte, []byte, error) {
	var sel tpm2.PCRSelection

	// Generate SRK
	srk, srkpub, _, _, _, _, err := tpm2.CreatePrimaryEx(conn, tpm2.HandleOwner, sel, "", "", DefaultSRKTemplate)
	if err != nil {
		return 0, nil, 0, nil, nil, err
	}

	// Generate AIK
	aikpriv, aikpub, _, _, _, err := tpm2.CreateKey(conn, srk, sel, "", "", DefaultAIKTemplate)
	if err != nil {
		_ = tpm2.FlushContext(conn, srk)
		return 0, nil, 0, nil, nil, err
	}

	// Load AIK
	aik, _, err := tpm2.Load(conn, srk, "", aikpub, aikpriv)
	if err != nil {
		_ = tpm2.FlushContext(conn, srk)
		return 0, nil, 0, nil, nil, err
	}

	return srk, srkpub, aik, aikpub, aikpriv, nil
}

func FlushKeys(conn io.ReadWriteCloser, srk, aik tpmutil.Handle) {
	if srk != 0 && srk != tpm2.HandleNull {
		_ = tpm2.FlushContext(conn, srk)
	}
	if aik != 0 && aik != tpm2.HandleNull {
		_ = tpm2.FlushContext(conn, aik)
	}
}

func FlushAllHandles(conn io.ReadWriteCloser) error {
	vals, _, err := tpm2.GetCapability(conn, tpm2.CapabilityHandles, 100, uint32(tpm2.HandleTypeTransient)<<24)
	if err != nil {
		return err
	}

	if len(vals) > 0 {
		for _, handle := range vals {
			switch t := handle.(type) {
			default:

			case tpmutil.Handle:
				_ = tpm2.FlushContext(conn, t)
			}
		}
	}

	return nil
}
