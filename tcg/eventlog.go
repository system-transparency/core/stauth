package tcg

import (
	"io/fs"

	"git.glasklar.is/system-transparency/core/stauth/metadata"
)

const (
	UefiEventLogPath = "sys/kernel/security/tpm0/binary_bios_measurements"
)

func ReadEventLogs(fsys fs.FS) ([]byte, []byte, error) {
	uefilog, err := fs.ReadFile(fsys, UefiEventLogPath)
	if err != nil {
		return nil, nil, err
	}
	stbootlog, err := metadata.Read(metadata.EventLog, fsys)
	if err != nil {
		stbootlog = nil
	}
	return uefilog, stbootlog, nil
}
