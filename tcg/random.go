package tcg

import "crypto/rand"

func RandomBuffer(bytes int) ([]byte, error) {
	var value = make([]byte, bytes)

	l, err := rand.Read(value)
	if l != bytes || err != nil {
		return []byte{}, err
	}

	return value, nil
}
