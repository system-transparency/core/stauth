package tcg

import (
	"errors"
	"io"

	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/google/go-tpm/tpmutil"
)

var (
	ErrNoSuchHandle = errors.New("no such handle")
)

func NvHandle(rw io.ReadWriter, handle tpmutil.Handle) error {
	caps, _, err := tpm2.GetCapability(rw, tpm2.CapabilityHandles, 1, uint32(handle))
	if err != nil {
		return err
	}

	if len(caps) != 1 {
		return ErrNoSuchHandle
	}
	if h, ok := caps[0].(tpmutil.Handle); !ok || h != handle {
		return ErrNoSuchHandle
	}

	return nil
}
