package tcg

import (
	"errors"
	"fmt"

	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/google/go-tpm/tpmutil"
)

// Comptes the TCG Name and Qualified Name of TPM 2.0 entities.
func ComputeName(path ...interface{}) (tpm2.Name, error) {
	// TPM 2.0 spec part 1, section 16
	// Name(PCR)       = Handle
	// Name(Session)   = Handle
	// Name(Permanent) = Handle
	// Name(NV Index)  = NameAlg || H_NameAlg(NVPublic)
	// Name(Object)    = NameAlg || H_NameAlg(Public)

	// TPM 2.0 spec part 1, section 26.5
	// QN(B) = H_NameAlg_B(QN(Parent(A)) || Name(B))
	// QN(Hierarchy) = Name(Hierarchy) = Hierarchy Handle
	var prevQN *tpm2.Name

	for _, entity := range path {
		var name tpm2.Name

		switch entity := entity.(type) {
		case tpmutil.Handle:
			handle := entity

			switch handle & 0xff000000 {
			// PCR, HMAC session, Policy session, Permanent values
			case 0x00000000, 0x02000000, 0x03000000, 0x40000000:
				name.Handle = &handle

				// NV Index
			default:
				return tpm2.Name{}, errors.New("need NVPublic to compute QName  of NV Index")
			}

		case tpm2.NVPublic:
			pub := entity
			blob, err := tpmutil.Pack(pub)
			if err != nil {
				return tpm2.Name{}, err
			}

			hsh, err := pub.NameAlg.Hash()
			if err != nil {
				return tpm2.Name{}, err
			}

			name.Digest.Value = hsh.New().Sum([]byte(blob))
			name.Digest.Alg = pub.NameAlg

		case tpm2.Public:
			pub := entity
			nam, err := pub.Name()
			if err != nil {
				return tpm2.Name{}, err
			}
			name = tpm2.Name(nam)

		case *tpm2.Public:
			pub := *entity
			nam, err := pub.Name()
			if err != nil {
				return tpm2.Name{}, err
			}
			name = tpm2.Name(nam)

		case tpm2.Name:
			name = tpm2.Name(entity)

		case *tpm2.Name:
			name = tpm2.Name(*entity)

		default:
			return tpm2.Name{}, fmt.Errorf("cannot compute Name of %#v", entity)
		}

		// special case: root entity
		if prevQN == nil {
			prevQN = &name
			continue
		}

		if name.Digest == nil {
			return tpm2.Name{}, errors.New("derived object is a handle")
		}

		// general case
		// QN(B) = H_NameAlg_B(QN(A) || Name(B))
		buf, err := tpm2.Name(name).Encode()
		if err != nil {
			return tpm2.Name{}, err
		}
		qbuf, err := tpm2.Name(*prevQN).Encode()
		if err != nil {
			return tpm2.Name{}, err
		}

		hshTy, err := name.Digest.Alg.Hash()
		if err != nil {
			return tpm2.Name{}, err
		}
		hsh := hshTy.New()
		hsh.Write(qbuf[2:])
		hsh.Write(buf[2:])

		prevQN.Handle = nil
		prevQN.Digest = &tpm2.HashValue{
			Value: hsh.Sum([]byte{}),
			Alg:   name.Digest.Alg,
		}
	}

	if prevQN == nil {
		return tpm2.Name{}, errors.New("no entities given")
	}

	return *prevQN, nil
}
