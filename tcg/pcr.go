package tcg

import (
	"fmt"
	"io"
	"sort"

	"github.com/google/go-tpm/legacy/tpm2"
)

var (
	maxCababilityBuffer = 1024
	maxTpmProperties    = ((maxCababilityBuffer - 8) / 8)
)

func capabilityPCRs(conn io.ReadWriteCloser) (pcrs []tpm2.PCRSelection, err error) {
	var tmp []interface{}
	more := true
	fa := uint32(0)
	for more {
		var newTmp []interface{}
		newTmp, more, err = tpm2.GetCapability(conn, tpm2.CapabilityPCRs, uint32(maxTpmProperties), fa)
		if err != nil {
			return nil, fmt.Errorf("tpm get capability (PCRs) failed: %w", err)
		}
		fa = uint32(newTmp[len(newTmp)-1].(tpm2.PCRSelection).Hash)
		tmp = append(tmp, newTmp...)
	}

	for _, t := range tmp {
		p := t.(tpm2.PCRSelection)
		pcrs = append(pcrs, p)
	}

	return
}

func PCRValues(conn io.ReadWriteCloser, sel []int32) ([][]byte, error) {
	// get available PCR banks and convert it to a map of PCRs per hash
	banks, err := capabilityPCRs(conn)
	if err != nil {
		return nil, err
	}

	var avail []int
	for _, bank := range banks {
		if bank.Hash == tpm2.AlgSHA256 {
			avail = bank.PCRs
			break
		}
	}
	sort.Ints(avail)
	sort.Slice(sel, func(i, j int) bool { return sel[i] < sel[j] })

	seenpcr := make([][]byte, sel[len(sel)-1]+1)

	// if a PCR does not return a value, check capabilities if it should be there
	for _, pcrIndex := range sel {
		pcr := int(pcrIndex)
		pcrSelection := tpm2.PCRSelection{
			Hash: tpm2.AlgSHA256,
			PCRs: []int{pcr},
		}
		pcrVals, err := tpm2.ReadPCRs(conn, pcrSelection)
		if err != nil {
			return nil, fmt.Errorf("unable to read PCRs from TPM: %v", err)
		}

		pcrVal, present := pcrVals[pcr]
		if !present {
			idx := sort.SearchInts(avail, pcr)
			if idx < len(avail) && avail[idx] == pcr {
				return nil, fmt.Errorf("PCR %d value missing from response", pcrIndex)
			}
		}

		seenpcr[pcrIndex] = pcrVal
	}

	return seenpcr, nil
}
