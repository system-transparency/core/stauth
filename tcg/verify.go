package tcg

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rsa"
	"crypto/sha256"
	"errors"

	"github.com/google/go-tpm/legacy/tpm2"
)

var ErrQuote = errors.New("quote invalid")

func Verify(quote tpm2.AttestationData, sig tpm2.Signature, pcr [][]byte, aik tpm2.Public) error {
	// hash up all pcr
	pcrHash := sha256.New()
	for _, p := range pcr {
		pcrHash.Write(p)
	}

	// match pcr against quote
	if quote.AttestedQuoteInfo == nil {
		return ErrQuote
	}

	info := quote.AttestedQuoteInfo

	pcrDigest := pcrHash.Sum(nil)
	if !bytes.Equal(info.PCRDigest, pcrDigest) {
		return ErrQuote
	}

	if quote.Magic != 0xFF544347 {
		return ErrQuote
	}

	// verify quote signature
	nameAlgHash, err := aik.NameAlg.Hash()
	if err != nil {
		return err
	}
	key, err := aik.Key()
	if err != nil {
		return err
	}

	attestBlob, err := quote.Encode()
	if err != nil {
		return err
	}

	attestHasher := nameAlgHash.New()
	attestHasher.Write(attestBlob)
	attestHash := attestHasher.Sum(nil)

	sigValid := false
	if sig.ECC != nil {
		ec, ok := (key).(*ecdsa.PublicKey)
		if !ok || ec.Curve != elliptic.P256() {
			return ErrQuote
		}
		sigValid = ecdsa.Verify(ec, attestHash, sig.ECC.R, sig.ECC.S)
	} else if sig.RSA != nil {
		pssOpt := rsa.PSSOptions{Hash: crypto.SHA256, SaltLength: rsa.PSSSaltLengthAuto}
		sigValid = rsa.VerifyPSS(key.(*rsa.PublicKey), crypto.SHA256, attestHash, sig.RSA.Signature, &pssOpt) == nil
	}

	if !sigValid {
		return ErrQuote
	}

	return nil
}
