package quotev0

import (
	"io"
	"io/fs"

	"git.glasklar.is/system-transparency/core/stauth/tcg"
	"github.com/google/go-tpm/legacy/tpm2"
)

//go:generate protoc --go_opt=module=git.glasklar.is/core/stauth/quotev0 --go_out=./ quotev0.proto

var (
	RequestContentType  = "application/protobuf; proto=quotev0.Request"
	ResponseContentType = "application/protobuf; proto=quotev0.Response"
)

func NewRequest(aikPub []byte, aikPriv []byte, pcr []int32) (*Request, error) {
	nonce, err := tcg.RandomBuffer(32)
	if err != nil {
		return nil, err
	}

	req := Request{
		AikPublic:  aikPub,
		AikPrivate: aikPriv,
		Nonce:      nonce,
		Pcr:        pcr,
	}

	return &req, nil
}

func NewResponse(conn io.ReadWriteCloser, fsys fs.FS, req *Request) (*Response, error) {
	srk, aik, err := tcg.LoadKeys(conn, req.AikPublic, req.AikPrivate)
	if err != nil {
		return nil, err
	}
	defer tcg.FlushKeys(conn, srk, aik)

	sel := tpm2.PCRSelection{
		Hash: tpm2.AlgSHA256,
		PCRs: make([]int, len(req.Pcr)),
	}
	for i, p := range req.Pcr {
		sel.PCRs[i] = int(p)
	}
	quote, sig, err := tpm2.Quote(conn, aik, "", "", req.Nonce, sel, tpm2.AlgNull)
	if err != nil {
		return nil, err
	}

	sigbuf, err := sig.Encode()
	if err != nil {
		return nil, err
	}

	pcrary, err := tcg.PCRValues(conn, req.Pcr)
	if err != nil {
		return nil, err
	}

	var pcr []*Response_PCR
	for i, p := range pcrary {
		if len(p) != 32 {
			continue
		}
		pcr = append(pcr, &Response_PCR{
			Index: int32(i),
			Value: p,
		})
	}

	// get event logs
	uefilog, stbootlog, err := tcg.ReadEventLogs(fsys)
	if err != nil {
		uefilog = nil
		stbootlog = nil
	}

	resp := Response{
		Quote:     quote,
		Signature: sigbuf,
		Pcr:       pcr,
		UefiLog:   uefilog,
		StbootLog: stbootlog,
	}

	return &resp, nil
}
