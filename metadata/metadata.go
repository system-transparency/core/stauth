package metadata

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"io/fs"
)

type Metadata struct {
	Cmdline string
}

const (
	UxIdentity = 0x44495855
	EventLog   = 0x474F4C45
)

var (
	ErrNotFound = errors.New("metadata not found")

	currentVersion = []byte("stboot metadata v1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")

	pmemGlobExpr = "dev/pmem*"
)

func Read(id uint32, fsys fs.FS) ([]byte, error) {
	nodes, err := fs.Glob(fsys, pmemGlobExpr)
	if err != nil {
		return nil, err
	}

	for _, node := range nodes {
		data, err := readFromNode(node, id, fsys)
		if err == nil {
			return data, nil
		}
	}

	return nil, ErrNotFound
}

func readFromNode(path string, id uint32, fsys fs.FS) ([]byte, error) {
	buf, err := fs.ReadFile(fsys, path)
	if err != nil {
		return nil, err
	}

	if !bytes.Equal(buf[:32], currentVersion) {
		return nil, ErrNotFound
	}

	fd := bytes.NewReader(buf[32:])
	for {
		var header struct {
			Id   uint32
			Size uint32
		}
		err = binary.Read(fd, binary.LittleEndian, &header)
		if err != nil {
			return nil, err
		}
		if int(header.Size) > len(buf) {
			return nil, errors.New("metadata size exceeds buffer size")
		}

		if header.Id == id {
			data := make([]byte, header.Size)
			_, err = fd.Read(data)
			if err != nil {
				return nil, err
			}

			return data, nil
		}

		_, err = fd.Seek(int64(header.Size), io.SeekCurrent)
		if err != nil {
			return nil, err
		}
	}
}
