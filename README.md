stauth: System Transparency Remote Attestation
==============================================

`stauth` allows administrators to request a cryptographic proof from an
`stboot`-booted server that it is running a specified set of code. The proof is
generated by a separate chip that is isolated from the host system. This
ensures that the proof cannot be forged, even if the server is running
malicious software. This proof, referred to as a "quote", encompasses the
firmware (UEFI/BIOS), bootloader (`stboot`), and the operating system (OS
package).

For `stauth` to be utilized, a server that operates with `stboot` and is
furnished with a Trusted Platform Module is mandatory. After setting up the
server, administrators can generate a quote on demand to validate the
following:

1. The server you are connecting to is owned by a specific service operator.
2. The server operates on a distinct version of `stboot` and has booted with an
   OS package provided by the server's proprietor.
3. The server's firmware remains unaltered since its last provisioning or
   update by the server's owner.

The `stauth` application employs endorsements to authenticate the values in
quotes. There are three categories of endorsements: for `stboot`, OS packages,
and the server platform. An endorsement from each category is essential for
quote verification. `stauth` generates these endorsements from the
corresponding artifact; for example, the ISO for `stboot` and the JSON
manifest/ZIP file combination for OS packages. The platform endorsement, which
includes the firmware, is created by executing `stauth` on the server that's
being verified. A secure device, such as the administrator's computer, is used
to both request and endorse the firmware through a bespoke HTTP-based protocol.

Usage
-----

The repository uses Git LFS that needs to be installed separately and checked out.

```bash
sudo apt install git-lfs
# sudo pacman -S git-lfs
# sudo brew install git-lfs
# dnf install git-lfs
git lfs clone git@git@git.glasklar.is:system-transparency/core/stauth.git
# or
git clone git@git.glasklar.is:system-transparency/core/stauth.git
cd stauth
git lfs pull
```

Then, the `stauth` binary can be build.

```bash
make
make test
```

To enroll a device, make sure it has an TPM 2.0. Then give it a human-readbale
name and use the enroll subcommand to get the initial measurements.

```bash
# on the device to be attested
ls -la /dev/tpm0
# crw-rw---- 1 tss root 10, 224 Apr  5 16:33 /dev/tpm0
sudo ./stauth id test.example.com
sudo ./stauth endorse --platform-server 0.0.0.0:8080

# on the operator's computer
./stauth endorse --platform <device hostname>:8080
```

Now, you need to create endorsements for the stboot ISO and os-pkg booted on
the device.

```bash
# on the operator's computer
./stauth endorse --stboot <path to stboot.iso>
./stauth endorse --ospkg-zip <path to os-pkg zip> --ospkg-json <path to os-pkg json>
```

Three files representing the endorsements should now be on the operators
computer: `*.stboot.pb`, `*.ospkg.pb` and `*.platform.pb`. With these and the
quote service running on the device to be attested a quote can be generated and
verified against the endorsements.

```bash
# on the device to be attested
ls -la /dev/tpm0
# crw-rw---- 1 tss root 10, 224 Apr  5 16:33 /dev/tpm0
sudo ./stauth quote host

# on the operator's computer
./stauth quote operator <device hostname>:8080 *.platform.pb *.stboot.pb *.ospkg.pb
```
