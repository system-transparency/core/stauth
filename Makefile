GO ?= go
GOFLAGS ?=
GOENV ?= CGO_ENABLED=0
GOTESTFLAGS ?= -coverpkg=./... -coverprofile=coverage.out -covermode count ./...

.PHONY: stauth clean test ci coverage generate

all: stauth

generate:
	$(GO) install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
	$(GO) install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
	$(GOENV) $(GO) generate ./...

stauth:
	$(GOENV) $(GO) build $(GOFLAGS) -o $@

test:
	$(GO) install gotest.tools/gotestsum@v1.10
	gotestsum -- $(GOTESTFLAGS)

coverage: test
	$(GO) tool cover -html=coverage.out

ci: stauth
	$(GO) install gotest.tools/gotestsum@v1.10
	$(GO) install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.53
	-gotestsum --junitfile unit-tests.xml --format testname -- $(GOTESTFLAGS)
	golangci-lint run
	$(GO) get github.com/boumenot/gocover-cobertura
	$(GO) run github.com/boumenot/gocover-cobertura < coverage.out > coverage.xml

clean:
	rm -f stauth coverage.out coverage.xml unit-tests.xml
