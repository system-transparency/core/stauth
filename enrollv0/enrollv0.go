package enrollv0

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rsa"
	"crypto/x509"
	"fmt"
	"io"

	"github.com/google/go-tpm/legacy/tpm2"
	google "github.com/google/go-tpm/legacy/tpm2/credactivation"

	"git.glasklar.is/system-transparency/core/stauth/tcg"
)

//go:generate protoc --go_opt=module=git.glasklar.is/core/stauth/enrollv0 --go_out=./ enrollv0.proto

const (
	RequestContentType   = "application/protobuf; proto=enrollv0.Request"
	AnswerContentType    = "application/protobuf; proto=enrollv0.Answer"
	ChallengeContentType = "application/protobuf; proto=enrollv0.Challenge"
)

// EK, AIK public key, EK certification, EK certificate
func NewRequest(conn io.ReadWriteCloser, ekcert *x509.Certificate, identity string) (*Request, error) {
	srk, srkpub, aik, aikpub, aikpriv, err := tcg.GenerateKeys(conn)
	if err != nil {
		return nil, err
	}
	tcg.FlushKeys(conn, srk, aik)

	ek, ekpub, err := tcg.EndorsementKey(conn)
	if err != nil {
		return nil, err
	}
	_ = tpm2.FlushContext(conn, ek)

	ekbuf, err := ekpub.Encode()
	if err != nil {
		return nil, err
	}

	ret := Request{
		EkPublic:   ekbuf,
		SrkPublic:  srkpub,
		AikPublic:  aikpub,
		AikPrivate: aikpriv,
		UxIdentity: identity,
	}

	if ekcert != nil {
		ret.EkCertificate = ekcert.Raw
	}

	return &ret, nil
}

// Create a challenge for the device to be enrolled.
// Run on the operator's machine.
func NewChallenge(req *Request) (*Challenge, []byte, error) {
	// parse EK
	ekpub, err := tpm2.DecodePublic(req.EkPublic)
	if err != nil {
		return nil, nil, fmt.Errorf("error decoding EK public key, %s", err)
	}
	ekkey, err := ekpub.Key()
	if err != nil {
		return nil, nil, fmt.Errorf("error decoding EK public key, %s", err)
	}

	// verify EK certificate
	if ekcert, err := x509.ParseCertificate(req.EkCertificate); err == nil {
		fmt.Printf("Endorse %s by %s\n", ekcert.Subject.CommonName, ekcert.Issuer.CommonName)
		// XXX: make sure it matches the EK pub key
		// XXX: TCG EK certificate extension
	}

	// verify AIK parameters
	aik, err := tpm2.DecodePublic(req.AikPublic)
	if err != nil {
		return nil, nil, fmt.Errorf("error decoding AIK public key, %s", err)
	}
	if aik.Type != tpm2.AlgECC {
		return nil, nil, fmt.Errorf("AIK is not ECC")
	}
	if aik.NameAlg != tpm2.AlgSHA256 {
		return nil, nil, fmt.Errorf("AIK name algorithm is not SHA256")
	}
	if aik.ECCParameters.CurveID != tpm2.CurveNISTP256 {
		return nil, nil, fmt.Errorf("AIK curve is not NISTP256")
	}
	if aik.ECCParameters.Sign == nil {
		return nil, nil, fmt.Errorf("AIK sign scheme is not ECDSA")
	}
	if aik.ECCParameters.Sign.Alg != tpm2.AlgECDSA {
		return nil, nil, fmt.Errorf("AIK sign scheme is not ECDSA")
	}
	if aik.ECCParameters.Sign.Hash != tpm2.AlgSHA256 {
		return nil, nil, fmt.Errorf("AIK sign scheme is not ECDSA")
	}
	if aik.Attributes != (tpm2.FlagFixedTPM |
		tpm2.FlagFixedParent |
		tpm2.FlagSensitiveDataOrigin |
		tpm2.FlagUserWithAuth |
		tpm2.FlagRestricted |
		tpm2.FlagSign) {
		return nil, nil, fmt.Errorf("AIK attributes are not correct")
	}

	// create AIK activation challange
	challengeNonce, err := tcg.RandomBuffer(32)
	if err != nil {
		return nil, nil, fmt.Errorf("error creating AIK certificate, %s", err)
	}

	// encrypt identity claim with EK
	var encCred, encSecret, ciphertext, nonce []byte
	switch ekpub := ekkey.(type) {
	case *rsa.PublicKey:
		encCred, encSecret, ciphertext, nonce, err = makeCredential(aik, ekpub, 0, challengeNonce)
		if err != nil {
			return nil, nil, fmt.Errorf("error encrypting AIK certificate, %s", err)
		}
	default:
		return nil, nil, fmt.Errorf("unsupported EK type")
	}

	ch := Challenge{
		AikPublic:           req.AikPublic,
		AikPrivate:          req.AikPrivate,
		EncryptedCredential: encCred,
		EncryptedSecret:     encSecret,
		Ciphertext:          ciphertext,
		Nonce:               nonce,
	}

	return &ch, challengeNonce, nil
}

func makeCredential(key tpm2.Public, ek *rsa.PublicKey, ekNameAlg tpm2.Algorithm, secret []byte) ([]byte, []byte, []byte, []byte, error) {
	credkey, err := tcg.RandomBuffer(16)
	if err != nil {
		return nil, nil, nil, nil, err
	}

	nonce, err := tcg.RandomBuffer(12)
	if err != nil {
		return nil, nil, nil, nil, err
	}

	block, err := aes.NewCipher(credkey)
	if err != nil {
		return nil, nil, nil, nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, nil, nil, nil, err
	}

	ciphertext := aesgcm.Seal(nil, nonce, secret, nil)

	name, err := key.Name()
	if err != nil {
		return nil, nil, nil, nil, err
	}

	// TPM2_ActivateCredential
	// 		activateHandle = Device key (AIK)
	//  	keyHandle = EK (protector)
	//  	creationData = encrypted X.509 certificate
	//  	secret = EK-encrypted seed for creationData
	// Generate
	//		aik = Device key (AIK)
	//		pub = EK (protector)
	//		pubNameAlg = EK Name algorithm
	//		symBlockSize
	//		secret = unencrypted secret
	encCred, encSecret, err := google.Generate(name.Digest, ek, 16, credkey)

	if err != nil {
		return nil, nil, nil, nil, err
	} else {
		return encCred[2:], encSecret, ciphertext, nonce, err
	}
}

func NewAnswer(conn io.ReadWriteCloser, ch *Challenge, uefilog, stbootlog []byte, dataChannel *x509.Certificate) (*Answer, error) {
	var endorsementAuth, auth string

	ek, _, err := tcg.EndorsementKey(conn)
	if err != nil {
		return nil, err
	}
	defer func() { _ = tpm2.FlushContext(conn, ek) }()

	srk, aik, err := tcg.LoadKeys(conn, ch.AikPublic, ch.AikPrivate)
	if err != nil {
		return nil, err
	}

	ekSession, _, err := tpm2.StartAuthSession(
		conn,
		tpm2.HandleNull,  /*tpmKey*/
		tpm2.HandleNull,  /*bindKey*/
		make([]byte, 16), /*nonceCaller*/
		nil,              /*secret*/
		tpm2.SessionPolicy,
		tpm2.AlgNull,
		tpm2.AlgSHA256)
	if err != nil {
		return nil, fmt.Errorf("creating ek session: %v", err)
	}

	defer func() { _ = tpm2.FlushContext(conn, ekSession) }()

	if len(ch.EncryptedSecret) < 2 {
		return nil, fmt.Errorf("credential secret not a TPM2B structure")
	}

	authCmd := tpm2.AuthCommand{
		Session:    tpm2.HandlePasswordSession,
		Attributes: tpm2.AttrContinueSession,
		Auth:       []byte(endorsementAuth),
	}

	_, _, err = tpm2.PolicySecret(conn, tpm2.HandleEndorsement, authCmd, ekSession, nil, nil, nil, 0)
	if err != nil {
		return nil, fmt.Errorf("tpm2.PolicySecret() failed: %v", err)
	}

	authCmds := []tpm2.AuthCommand{
		{Session: tpm2.HandlePasswordSession, Attributes: tpm2.AttrContinueSession, Auth: []byte(auth)},
		{Session: ekSession, Attributes: tpm2.AttrContinueSession},
	}
	certKey, err := tpm2.ActivateCredentialUsingAuth(
		conn, authCmds, aik, ek, ch.EncryptedCredential, ch.EncryptedSecret[2:])
	tcg.FlushKeys(conn, srk, aik)
	if err != nil {
		return nil, fmt.Errorf("activate device key certificate: %v", err)
	}
	block, err := aes.NewCipher(certKey)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	c, err := aesgcm.Open(nil, ch.Nonce, ch.Ciphertext, nil)
	if err != nil {
		return nil, err
	}

	var dataChannelRaw []byte
	if dataChannel != nil {
		dataChannelRaw = dataChannel.Raw
	}

	ret := Answer{
		Nonce:               c,
		UefiEventLog:        uefilog,
		StbootEventLog:      stbootlog,
		DataChannelIdentity: dataChannelRaw,
	}
	return &ret, nil
}
