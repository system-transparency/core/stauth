// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.5.0
// source: enrollv0.proto

package enrollv0

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// enrollemnt request sent from the device to the operator
type Request struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	EkCertificate []byte `protobuf:"bytes,1,opt,name=ek_certificate,json=ekCertificate,proto3" json:"ek_certificate,omitempty"`
	EkPublic      []byte `protobuf:"bytes,2,opt,name=ek_public,json=ekPublic,proto3" json:"ek_public,omitempty"`
	SrkPublic     []byte `protobuf:"bytes,3,opt,name=srk_public,json=srkPublic,proto3" json:"srk_public,omitempty"`
	AikPublic     []byte `protobuf:"bytes,4,opt,name=aik_public,json=aikPublic,proto3" json:"aik_public,omitempty"`
	AikPrivate    []byte `protobuf:"bytes,5,opt,name=aik_private,json=aikPrivate,proto3" json:"aik_private,omitempty"`
	UxIdentity    string `protobuf:"bytes,6,opt,name=ux_identity,json=uxIdentity,proto3" json:"ux_identity,omitempty"`
}

func (x *Request) Reset() {
	*x = Request{}
	if protoimpl.UnsafeEnabled {
		mi := &file_enrollv0_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Request) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Request) ProtoMessage() {}

func (x *Request) ProtoReflect() protoreflect.Message {
	mi := &file_enrollv0_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Request.ProtoReflect.Descriptor instead.
func (*Request) Descriptor() ([]byte, []int) {
	return file_enrollv0_proto_rawDescGZIP(), []int{0}
}

func (x *Request) GetEkCertificate() []byte {
	if x != nil {
		return x.EkCertificate
	}
	return nil
}

func (x *Request) GetEkPublic() []byte {
	if x != nil {
		return x.EkPublic
	}
	return nil
}

func (x *Request) GetSrkPublic() []byte {
	if x != nil {
		return x.SrkPublic
	}
	return nil
}

func (x *Request) GetAikPublic() []byte {
	if x != nil {
		return x.AikPublic
	}
	return nil
}

func (x *Request) GetAikPrivate() []byte {
	if x != nil {
		return x.AikPrivate
	}
	return nil
}

func (x *Request) GetUxIdentity() string {
	if x != nil {
		return x.UxIdentity
	}
	return ""
}

// sent by the operator to the device as response to a Request
type Challenge struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AikPublic           []byte `protobuf:"bytes,1,opt,name=aik_public,json=aikPublic,proto3" json:"aik_public,omitempty"`
	AikPrivate          []byte `protobuf:"bytes,2,opt,name=aik_private,json=aikPrivate,proto3" json:"aik_private,omitempty"`
	EncryptedCredential []byte `protobuf:"bytes,3,opt,name=encrypted_credential,json=encryptedCredential,proto3" json:"encrypted_credential,omitempty"`
	EncryptedSecret     []byte `protobuf:"bytes,4,opt,name=encrypted_secret,json=encryptedSecret,proto3" json:"encrypted_secret,omitempty"`
	Ciphertext          []byte `protobuf:"bytes,5,opt,name=ciphertext,proto3" json:"ciphertext,omitempty"`
	Nonce               []byte `protobuf:"bytes,6,opt,name=nonce,proto3" json:"nonce,omitempty"`
}

func (x *Challenge) Reset() {
	*x = Challenge{}
	if protoimpl.UnsafeEnabled {
		mi := &file_enrollv0_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Challenge) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Challenge) ProtoMessage() {}

func (x *Challenge) ProtoReflect() protoreflect.Message {
	mi := &file_enrollv0_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Challenge.ProtoReflect.Descriptor instead.
func (*Challenge) Descriptor() ([]byte, []int) {
	return file_enrollv0_proto_rawDescGZIP(), []int{1}
}

func (x *Challenge) GetAikPublic() []byte {
	if x != nil {
		return x.AikPublic
	}
	return nil
}

func (x *Challenge) GetAikPrivate() []byte {
	if x != nil {
		return x.AikPrivate
	}
	return nil
}

func (x *Challenge) GetEncryptedCredential() []byte {
	if x != nil {
		return x.EncryptedCredential
	}
	return nil
}

func (x *Challenge) GetEncryptedSecret() []byte {
	if x != nil {
		return x.EncryptedSecret
	}
	return nil
}

func (x *Challenge) GetCiphertext() []byte {
	if x != nil {
		return x.Ciphertext
	}
	return nil
}

func (x *Challenge) GetNonce() []byte {
	if x != nil {
		return x.Nonce
	}
	return nil
}

// sent by the device to the operator as response to a Challenge
type Answer struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Nonce               []byte `protobuf:"bytes,1,opt,name=nonce,proto3" json:"nonce,omitempty"`
	UefiEventLog        []byte `protobuf:"bytes,2,opt,name=uefi_event_log,json=uefiEventLog,proto3" json:"uefi_event_log,omitempty"`
	StbootEventLog      []byte `protobuf:"bytes,3,opt,name=stboot_event_log,json=stbootEventLog,proto3" json:"stboot_event_log,omitempty"`
	DataChannelIdentity []byte `protobuf:"bytes,4,opt,name=data_channel_identity,json=dataChannelIdentity,proto3" json:"data_channel_identity,omitempty"`
}

func (x *Answer) Reset() {
	*x = Answer{}
	if protoimpl.UnsafeEnabled {
		mi := &file_enrollv0_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Answer) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Answer) ProtoMessage() {}

func (x *Answer) ProtoReflect() protoreflect.Message {
	mi := &file_enrollv0_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Answer.ProtoReflect.Descriptor instead.
func (*Answer) Descriptor() ([]byte, []int) {
	return file_enrollv0_proto_rawDescGZIP(), []int{2}
}

func (x *Answer) GetNonce() []byte {
	if x != nil {
		return x.Nonce
	}
	return nil
}

func (x *Answer) GetUefiEventLog() []byte {
	if x != nil {
		return x.UefiEventLog
	}
	return nil
}

func (x *Answer) GetStbootEventLog() []byte {
	if x != nil {
		return x.StbootEventLog
	}
	return nil
}

func (x *Answer) GetDataChannelIdentity() []byte {
	if x != nil {
		return x.DataChannelIdentity
	}
	return nil
}

var File_enrollv0_proto protoreflect.FileDescriptor

var file_enrollv0_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x65, 0x6e, 0x72, 0x6f, 0x6c, 0x6c, 0x76, 0x30, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x08, 0x65, 0x6e, 0x72, 0x6f, 0x6c, 0x6c, 0x76, 0x30, 0x22, 0xcd, 0x01, 0x0a, 0x07, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x25, 0x0a, 0x0e, 0x65, 0x6b, 0x5f, 0x63, 0x65, 0x72,
	0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0d,
	0x65, 0x6b, 0x43, 0x65, 0x72, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x65, 0x12, 0x1b, 0x0a,
	0x09, 0x65, 0x6b, 0x5f, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0c,
	0x52, 0x08, 0x65, 0x6b, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x12, 0x1d, 0x0a, 0x0a, 0x73, 0x72,
	0x6b, 0x5f, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x09,
	0x73, 0x72, 0x6b, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x69, 0x6b,
	0x5f, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x09, 0x61,
	0x69, 0x6b, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x12, 0x1f, 0x0a, 0x0b, 0x61, 0x69, 0x6b, 0x5f,
	0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0a, 0x61,
	0x69, 0x6b, 0x50, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x75, 0x78, 0x5f,
	0x69, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a,
	0x75, 0x78, 0x49, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x22, 0xdf, 0x01, 0x0a, 0x09, 0x43,
	0x68, 0x61, 0x6c, 0x6c, 0x65, 0x6e, 0x67, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x69, 0x6b, 0x5f,
	0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x09, 0x61, 0x69,
	0x6b, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x12, 0x1f, 0x0a, 0x0b, 0x61, 0x69, 0x6b, 0x5f, 0x70,
	0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0a, 0x61, 0x69,
	0x6b, 0x50, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x12, 0x31, 0x0a, 0x14, 0x65, 0x6e, 0x63, 0x72,
	0x79, 0x70, 0x74, 0x65, 0x64, 0x5f, 0x63, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x61, 0x6c,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x13, 0x65, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x65,
	0x64, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x61, 0x6c, 0x12, 0x29, 0x0a, 0x10, 0x65,
	0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x65, 0x64, 0x5f, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0f, 0x65, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x65, 0x64,
	0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x63, 0x69, 0x70, 0x68, 0x65, 0x72,
	0x74, 0x65, 0x78, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0a, 0x63, 0x69, 0x70, 0x68,
	0x65, 0x72, 0x74, 0x65, 0x78, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6e, 0x6f, 0x6e, 0x63, 0x65, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x05, 0x6e, 0x6f, 0x6e, 0x63, 0x65, 0x22, 0xa2, 0x01, 0x0a,
	0x06, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x12, 0x14, 0x0a, 0x05, 0x6e, 0x6f, 0x6e, 0x63, 0x65,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x05, 0x6e, 0x6f, 0x6e, 0x63, 0x65, 0x12, 0x24, 0x0a,
	0x0e, 0x75, 0x65, 0x66, 0x69, 0x5f, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x5f, 0x6c, 0x6f, 0x67, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0c, 0x75, 0x65, 0x66, 0x69, 0x45, 0x76, 0x65, 0x6e, 0x74,
	0x4c, 0x6f, 0x67, 0x12, 0x28, 0x0a, 0x10, 0x73, 0x74, 0x62, 0x6f, 0x6f, 0x74, 0x5f, 0x65, 0x76,
	0x65, 0x6e, 0x74, 0x5f, 0x6c, 0x6f, 0x67, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0e, 0x73,
	0x74, 0x62, 0x6f, 0x6f, 0x74, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x4c, 0x6f, 0x67, 0x12, 0x32, 0x0a,
	0x15, 0x64, 0x61, 0x74, 0x61, 0x5f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x5f, 0x69, 0x64,
	0x65, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x13, 0x64, 0x61,
	0x74, 0x61, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x49, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x74,
	0x79, 0x42, 0x26, 0x5a, 0x24, 0x67, 0x69, 0x74, 0x2e, 0x67, 0x6c, 0x61, 0x73, 0x6b, 0x6c, 0x61,
	0x72, 0x2e, 0x69, 0x73, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f, 0x73, 0x74, 0x61, 0x75, 0x74, 0x68,
	0x2f, 0x65, 0x6e, 0x72, 0x6f, 0x6c, 0x6c, 0x76, 0x30, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x33,
}

var (
	file_enrollv0_proto_rawDescOnce sync.Once
	file_enrollv0_proto_rawDescData = file_enrollv0_proto_rawDesc
)

func file_enrollv0_proto_rawDescGZIP() []byte {
	file_enrollv0_proto_rawDescOnce.Do(func() {
		file_enrollv0_proto_rawDescData = protoimpl.X.CompressGZIP(file_enrollv0_proto_rawDescData)
	})
	return file_enrollv0_proto_rawDescData
}

var file_enrollv0_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_enrollv0_proto_goTypes = []interface{}{
	(*Request)(nil),   // 0: enrollv0.Request
	(*Challenge)(nil), // 1: enrollv0.Challenge
	(*Answer)(nil),    // 2: enrollv0.Answer
}
var file_enrollv0_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_enrollv0_proto_init() }
func file_enrollv0_proto_init() {
	if File_enrollv0_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_enrollv0_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Request); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_enrollv0_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Challenge); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_enrollv0_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Answer); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_enrollv0_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_enrollv0_proto_goTypes,
		DependencyIndexes: file_enrollv0_proto_depIdxs,
		MessageInfos:      file_enrollv0_proto_msgTypes,
	}.Build()
	File_enrollv0_proto = out.File
	file_enrollv0_proto_rawDesc = nil
	file_enrollv0_proto_goTypes = nil
	file_enrollv0_proto_depIdxs = nil
}
