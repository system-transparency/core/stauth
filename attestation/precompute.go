package attestation

import (
	"bytes"
	"compress/gzip"
	"crypto/sha256"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"

	"github.com/cavaliergopher/cpio"
	"github.com/diskfs/go-diskfs"
	"github.com/google/go-tpm/legacy/tpm2"
	saferwall "github.com/saferwall/pe"

	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/eventlog"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
)

var (
	DefaultPCRList = []int32{
		// firmware
		0, 1, 2, 3, 4, 5, 6, 7,
		// stboot
		8,
		// systemd-stub / UKI
		11,
		// stboot
		12, 13, 14,
	}
	stmgrVfatRegexp = regexp.MustCompile("STMGR.*_VFAT")
)

// XXX: copied from system-transparency/stboot/trust
type trustPolicy struct {
	SignatureThreshold int    `json:"ospkg_signature_threshold"`
	FetchMethod        string `json:"ospkg_fetch_method"`
}

func NewPlatform(uefibinlog []byte, stbootbinlog []byte, srkPub, aikPub tpm2.Public, aikPriv []byte, identity string, dataChannel *x509.Certificate) (*endorsev0.Platform, error) {
	uefilog, err := eventlog.ParseEventLog(uefibinlog)
	if err != nil {
		return nil, err
	}
	stbootlog, err := eventlog.ParseEventLog(stbootbinlog)
	if err != nil {
		if len(stbootbinlog) > 0 {
			return nil, err
		} else {
			stbootlog = nil
		}
	}

	events := uefilog.Events(eventlog.HashSHA256)
	if stbootlog != nil {
		events = append(events, stbootlog.Events(eventlog.HashSHA256)...)
	}

	tmpl, err := MakeTemplate(events, true)
	if err != nil {
		return nil, err
	}

	nam, err := tcg.ComputeName(tpm2.HandleEndorsement, srkPub, aikPub)
	if err != nil {
		return nil, fmt.Errorf("error computing name, %s", err)
	}

	nambuf, err := nam.Encode()
	if err != nil {
		return nil, fmt.Errorf("error encoding name, %s", err)
	}

	keybuf, err := aikPub.Encode()
	if err != nil {
		return nil, err
	}

	var dataChannelRaw []byte
	if dataChannel != nil {
		dataChannelRaw = dataChannel.Raw
	}

	ret := endorsev0.Platform{
		Log:         tmpl,
		AikPublic:   keybuf,
		AikPrivate:  aikPriv,
		AikQname:    nambuf,
		UxIdentity:  identity,
		DataChannel: dataChannelRaw,
	}
	return &ret, nil
}

func NewStboot(stbootIsoPath string) (*endorsev0.Stboot, error) {
	isoDisk, err := diskfs.Open(stbootIsoPath)
	if err != nil {
		return nil, err
	}

	isoFs, err := isoDisk.GetFilesystem(0)
	if err != nil {
		return nil, err
	}

	fatList, err := isoFs.ReadDir("VFAT")
	if err != nil {
		return nil, err
	}

	fatPath := ""
	for _, f := range fatList {
		if stmgrVfatRegexp.MatchString(f.Name()) {
			fatPath = fmt.Sprintf("VFAT/%s", f.Name())
			break
		}
	}

	if fatPath == "" {
		return nil, fmt.Errorf("stboot.iso has no embedded FAT")
	}

	fatCopy, err := os.CreateTemp("", "stmgr.*_vfat")
	if err != nil {
		return nil, err
	}
	defer fatCopy.Close()
	fatSrc, err := isoFs.OpenFile(fatPath, os.O_RDONLY)
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(fatCopy, fatSrc)
	if err != nil {
		return nil, err
	}
	fatSrc.Close()

	fatDisk, err := diskfs.Open(fatCopy.Name())
	if err != nil {
		return nil, err
	}
	fatFs, err := fatDisk.GetFilesystem(0)
	if err != nil {
		return nil, err
	}

	stboot, err := fatFs.OpenFile("/EFI/BOOT/BOOTX64.EFI", os.O_RDONLY)
	if err != nil {
		return nil, err
	}
	defer stboot.Close()

	bootx64, err := io.ReadAll(stboot)
	if err != nil {
		return nil, err
	}

	bootx64pe, err := parsePE(bootx64)
	if err != nil {
		return nil, err
	}

	ret := endorsev0.Stboot{
		Uki: bootx64pe.Authentihash(),
	}

	for _, sec := range bootx64pe.Sections {
		start := sec.Header.PointerToRawData
		end := start + sec.Header.VirtualSize
		sum := sha256.Sum256(bootx64[start:end])

		switch fmt.Sprintf("%s", sec.Header.Name) {
		case ".linux\x00\x00":
			linux, err := parsePE(bootx64[start:end])
			if err != nil {
				return nil, err
			}
			ret.Authentihash = linux.Authentihash()
			ret.Linux = sum[:]
		case ".osrel\x00\x00":
			ret.Osrel = sum[:]
		case ".cmdline":
			ret.Cmdline = sum[:]
		case ".initrd\x00":
			ret.Initrd = sum[:]

			rdgz, err := gzip.NewReader(bytes.NewBuffer(bootx64[start:end]))
			if err != nil {
				return nil, err
			}
			rd := cpio.NewReader(rdgz)

			for h, err := rd.Next(); err == nil; h, err = rd.Next() {
				contents, err := io.ReadAll(rd)
				if err != nil {
					return nil, err
				}

				switch h.Name {
				case "etc/ssl/certs/isrgrootx1.pem":
					certs, err := decodePEM(contents)
					if err != nil {
						return nil, err
					}
					for _, cert := range certs {
						ret.HttpsRoots = append(ret.HttpsRoots, cert.Raw)
					}

				case "etc/trust_policy/ospkg_signing_root.pem":
					blk, _ := pem.Decode(contents)
					cert, err := x509.ParseCertificate(blk.Bytes)
					if err != nil {
						return nil, err
					}
					ret.SigningRoot = cert.Raw
				case "etc/trust_policy/trust_policy.json":
					var trustPolicy trustPolicy
					err := json.Unmarshal(contents, &trustPolicy)
					if err != nil {
						return nil, err
					}
					contents, err = json.Marshal(trustPolicy)
					if err != nil {
						return nil, err
					}
					ret.SecurityConfig = contents
				}
			}
		}
	}

	return &ret, nil
}

func NewOsPkg(osPkgZipPath, osPkgJsonPath string) (*endorsev0.OsPkg, error) {
	var ret endorsev0.OsPkg

	ospkgZip, err := os.Open(osPkgZipPath)
	if err != nil {
		return nil, err
	}
	defer ospkgZip.Close()
	hs := sha256.New()

	_, err = io.Copy(hs, ospkgZip)
	if err != nil {
		return nil, err
	}
	ret.Zip = hs.Sum(nil)
	hs.Reset()

	ospkgJson, err := os.Open(osPkgJsonPath)
	if err != nil {
		return nil, err
	}
	defer ospkgJson.Close()

	ret.Descriptor_, err = io.ReadAll(ospkgJson)
	if err != nil {
		return nil, err
	}

	return &ret, nil
}

func parsePE(buf []byte) (*saferwall.File, error) {
	f, err := saferwall.NewBytes(buf, &saferwall.Options{})
	if err != nil {
		return nil, err
	}
	err = f.Parse()
	if err != nil {
		return nil, err
	}

	return f, nil
}

func decodePEM(pemBytes []byte) ([]*x509.Certificate, error) {
	var certs []*x509.Certificate

	for len(pemBytes) > 0 {
		block, rest := pem.Decode(pemBytes)
		if block == nil {
			break
		}

		if block.Type != "CERTIFICATE" {
			pemBytes = rest

			continue
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, err
		}

		certs = append(certs, cert)
		pemBytes = rest
	}

	if len(certs) == 0 {
		return nil, errors.New("no PEM block of type CERTIFICATE found")
	}

	return certs, nil
}
