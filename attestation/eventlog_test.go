package attestation

import (
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/stretchr/testify/assert"

	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/eventlog"
	"git.glasklar.is/system-transparency/core/stauth/metadata"
)

func TestPrecomputation(t *testing.T) {
	// dummy AIK
	aik := tpm2.Public{
		Type: tpm2.AlgRSA,
		RSAParameters: &tpm2.RSAParams{
			Sign: &tpm2.SigScheme{
				Alg:  tpm2.AlgRSASSA,
				Hash: tpm2.AlgSHA256,
			},
			KeyBits:     2048,
			ExponentRaw: 0x10001,
			ModulusRaw:  make([]byte, 256),
		},
		NameAlg: tpm2.AlgSHA256,
	}

	// compute the expocted os-pkg measurements from the files
	ospkg, err := NewOsPkg("../testdata/os-pkg-example-ubuntu20.zip", "../testdata/os-pkg-example-ubuntu20.json")
	assert.NoError(t, err)

	// compute the expoected stboot measurements from the iso
	stboot, err := NewStboot("../testdata/stboot.iso")
	assert.NoError(t, err)

	t.Run("Non TXT", func(t *testing.T) {
		// measurements collected off the machine
		uefilog, err := os.ReadFile("../testdata/log-nontxt.binary")
		assert.NoError(t, err)
		mbin, err := os.ReadFile("../testdata/metadata-nontxt.binary")
		assert.NoError(t, err)
		mockFs := make(fstest.MapFS)
		mockFs["dev/pmem0"] = &fstest.MapFile{Data: mbin, Mode: 0644}
		stlog, err := metadata.Read(metadata.EventLog, mockFs)
		assert.NoError(t, err)

		// extract the firmware measurements from the TPM 2.0 event log
		platform, err := NewPlatform(uefilog, stlog, aik, aik, nil, "text.example.com", nil)
		assert.NoError(t, err)

		// compute expected pcr values
		pcr, err := Reconstruct(DefaultPCRList, platform, stboot, ospkg)
		assert.NoError(t, err)

		// values form the device (VM, really)
		expectedPcr := map[int32]string{
			0:  "A7EAA2EF64FE37E23CC3E4230EF119C8805ABDF89E03191924EE38F8361F8EB5",
			1:  "4A64027F367A00F6B9F7BEA6B5381F763940921E1AE8E0A905EBE687B3ED760F",
			2:  "E3CBF180A079EA20D9431E4A07DB22ABED744C1E66F2FCC9872F092021082C73",
			3:  "3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969",
			4:  "214c4aeab1fa84b11221e3a5bfe389a07a95d3107e108add5488253e60137ab2",
			5:  "A5CEB755D043F32431D63E39F5161464620A3437280494B5850DC1B47CC074E0",
			6:  "3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969",
			7:  "B5710BF57D25623E4019027DA116821FA99F5C81E9E38B87671CC574F9281439",
			8:  "0000000000000000000000000000000000000000000000000000000000000000",
			11: "cc1d8d5fc7e80581ae0c6a64cc1f795581a8fc46d0e5a6b5dbbecc8d221fd3a8",
			12: "434b3074f36987fc2ac697ea8bc03a7b5518b3c37044dde5c06a5206850a5ce4",
			13: "231cd039ee37e20bce638a5761dfb6012daa127ae5c33b39d774bce961d53ff5",
			14: "e3ab318d1fa87562f33eec5d6c8c828e44185b88db6c5ea5a7a3614a642f7602",
		}

		// compare with precomputed values
		for i := range pcr {
			b, err := hex.DecodeString(expectedPcr[i])
			assert.NoError(t, err)
			assert.Equal(t, pcr[i], b, fmt.Sprintf("pcr %d", i))
		}
	})
}

func TestTemplate(t *testing.T) {
	files := []string{
		"../testdata/log-nontxt.binary",
		"../testdata/log-noshim.binary",
	}

	mbin, err := os.ReadFile("../testdata/metadata-nontxt.binary")
	assert.NoError(t, err)
	mockFs := make(fstest.MapFS)
	mockFs["dev/pmem0"] = &fstest.MapFile{Data: mbin, Mode: 0644}

	stbin, err := metadata.Read(metadata.EventLog, mockFs)
	assert.NoError(t, err)

	stlog, err := eventlog.ParseEventLog(stbin)
	assert.NoError(t, err)

	for _, f := range files {
		for _, withst := range []bool{false, true} {
			t.Run(fmt.Sprintf("%s withst=%v", f, withst), func(t *testing.T) {
				bin, err := os.ReadFile(f)
				assert.NoError(t, err)

				log, err := eventlog.ParseEventLog(bin)
				assert.NoError(t, err)

				var events []eventlog.Event
				if withst {
					events = append(log.Events(eventlog.HashSHA256), stlog.Events(eventlog.HashSHA256)...)
				} else {
					events = log.Events(eventlog.HashSHA256)
				}

				template, err := MakeTemplate(events, true)
				assert.NoError(t, err)

				if withst {
					assert.Equal(t, len(events)+numPCR, len(template))
				} else {
					assert.Equal(t, len(events)+7+numPCR, len(template))
				}

				var uki, linux, initrd, cmdline, osrel, authentihash, descriptor, securityconfig, signingroot, oszip, identity, datachannel bool
				for _, ent := range template {
					assert.NotEqual(t, ent.Type, endorsev0.Platform_Entry_UNSPECIFIED)
					switch ent.Type {
					case endorsev0.Platform_Entry_STBOOT_UKI:
						assert.False(t, uki)
						uki = true
					case endorsev0.Platform_Entry_STBOOT_LINUX:
						assert.False(t, linux)
						linux = true
					case endorsev0.Platform_Entry_STBOOT_INITRD:
						assert.False(t, initrd)
						initrd = true
					case endorsev0.Platform_Entry_STBOOT_CMDLINE:
						assert.False(t, cmdline)
						cmdline = true
					case endorsev0.Platform_Entry_STBOOT_OSREL:
						assert.False(t, osrel)
						osrel = true
					case endorsev0.Platform_Entry_STBOOT_AUTHENTIHASH:
						assert.False(t, authentihash)
						authentihash = true
					case endorsev0.Platform_Entry_OSPKG_DESCRIPTOR:
						assert.False(t, descriptor)
						descriptor = true
					case endorsev0.Platform_Entry_OSPKG_SECURITYCONFIG:
						assert.False(t, securityconfig)
						securityconfig = true
					case endorsev0.Platform_Entry_OSPKG_SIGNINGROOT:
						assert.False(t, signingroot)
						signingroot = true
					case endorsev0.Platform_Entry_OSPKG_ZIP:
						assert.False(t, oszip)
						oszip = true
					case endorsev0.Platform_Entry_UX_IDENTITY:
						assert.False(t, identity)
						identity = true
					case endorsev0.Platform_Entry_DATA_CHANNEL:
						assert.False(t, datachannel)
						datachannel = true
					}
				}

				assert.True(t, uki, "uki")

				if strings.Contains(f, "noshim") {
					assert.False(t, linux, "linux")
					assert.False(t, initrd, "initrd")
					assert.False(t, cmdline, "cmdline")
					assert.False(t, osrel, "osrel")
					assert.False(t, authentihash, "authentihash")
				} else {
					assert.True(t, uki, "uki")
					assert.True(t, linux, "linux")
					assert.True(t, initrd, "initrd")
					assert.True(t, cmdline, "cmdline")
					assert.True(t, osrel, "osrel")
					assert.True(t, authentihash, "authentihash")
				}
				assert.True(t, descriptor, "descriptor")
				assert.True(t, securityconfig, "securityconfig")
				assert.True(t, signingroot, "signingroot")
				assert.True(t, oszip, "oszip")
				assert.True(t, identity, "identity")
				assert.True(t, datachannel, "datachannel")
			})
		}
	}
}
