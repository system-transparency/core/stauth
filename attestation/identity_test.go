package attestation

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"fmt"
	"math/big"
	"testing"
	"testing/fstest"
	"time"

	"git.glasklar.is/system-transparency/core/stauth/efi"
	"github.com/google/go-tpm-tools/simulator"
	"github.com/stretchr/testify/assert"
)

func TestSetUxIdentity(t *testing.T) {
	ctx := context.Background()

	sim, err := simulator.Get()
	assert.NoError(t, err)

	defer sim.Close()

	id := "test.example.com"
	err = OverwriteIdentity(ctx, sim, id)
	assert.NoError(t, err)

	id3, err := Identity(ctx, sim)
	assert.NoError(t, err)
	assert.Equal(t, id, id3)

	id = "blub"
	err = OverwriteIdentity(ctx, sim, id)
	assert.NoError(t, err)

	id4, err := Identity(ctx, sim)
	assert.NoError(t, err)
	assert.Equal(t, id, id4)
}

func TestEmptyUxIdentity(t *testing.T) {
	ctx := context.Background()

	sim, err := simulator.Get()
	assert.NoError(t, err)

	defer sim.Close()

	_, err = Identity(ctx, sim)
	assert.Error(t, err)

	err = OverwriteIdentity(ctx, sim, "123")
	assert.NoError(t, err)

	err = OverwriteIdentity(ctx, sim, "")
	assert.Error(t, err)

	id5, err := Identity(ctx, sim)
	assert.NoError(t, err)
	assert.Equal(t, "123", id5)
}

func TestRemoveUxIdentity(t *testing.T) {
	ctx := context.Background()

	sim, err := simulator.Get()
	assert.NoError(t, err)

	defer sim.Close()

	id := "test.example.com"
	err = OverwriteIdentity(ctx, sim, id)
	assert.NoError(t, err)

	err = RemoveIdentity(ctx, sim)
	assert.NoError(t, err)

	_, err = Identity(ctx, sim)
	assert.Error(t, err)
}

func TestDataChannelIdentity(t *testing.T) {
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	assert.NoError(t, err)

	tomorrow := time.Now().Add(24 * time.Hour)
	tmpl := x509.Certificate{
		SignatureAlgorithm: x509.PureEd25519,
		SerialNumber:       big.NewInt(1),
		PublicKey:          pub,
		Subject:            pkix.Name{CommonName: "Test Subject"},
		NotAfter:           tomorrow,
		NotBefore:          time.Now(),
	}

	bin, err := x509.CreateCertificate(rand.Reader, &tmpl, &tmpl, pub, priv)
	assert.NoError(t, err)

	vari := make([]byte, len(bin)+4)
	copy(vari[4:], bin)

	path := fmt.Sprintf("%s/%s-%s", efi.EfivarfsMountPoint, DataChannelPublic, VaribleGuid.String())
	mockFs := make(fstest.MapFS)
	mockFs[path] = &fstest.MapFile{Data: vari, Mode: 0644}

	cert, err := DataChannel(mockFs)
	assert.NoError(t, err)

	assert.Equal(t, cert.Raw, bin)
}

func TestNoDataChannelIdentity(t *testing.T) {
	mockFs := make(fstest.MapFS)

	_, err := DataChannel(mockFs)
	assert.Error(t, err)
}

func TestInvalidDataChannelIdentity(t *testing.T) {
	path := fmt.Sprintf("%s/%s-%s", efi.EfivarfsMountPoint, DataChannelPublic, VaribleGuid.String())
	mockFs := make(fstest.MapFS)
	mockFs[path] = &fstest.MapFile{Data: []byte{1, 2, 3, 4, 5, 6, 7, 8}, Mode: 0644}

	_, err := DataChannel(mockFs)
	assert.Error(t, err)
}
