package attestation

import (
	"bytes"
	"context"
	"crypto/sha256"
	"errors"
	"fmt"
	"strings"

	"github.com/rs/zerolog"
	"github.com/sergi/go-diff/diffmatchpatch"

	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/eventlog"
	"git.glasklar.is/system-transparency/core/stauth/quotev0"
)

const (
	numPCR = 24
)

var (
	ErrFormat = errors.New("format invalid")

	linuxName   = sha256.Sum256([]byte(".linux\x00"))
	initrdName  = sha256.Sum256([]byte(".initrd\x00"))
	cmdlineName = sha256.Sum256([]byte(".cmdline\x00"))
	osrelName   = sha256.Sum256([]byte(".osrel\x00"))

	zeroPCR         = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	foxPCR          = []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}
	startupLocality = []byte("StartupLocality\x00")
)

func MakeTemplate(events []eventlog.Event, removeValues bool) ([]*endorsev0.Platform_Entry, error) {
	optionalValue := func(value []byte) []byte {
		if removeValues {
			return nil
		} else {
			return value
		}
	}

	log, err := eventlog.ParseEvents(events)
	if err != nil {
		return nil, err
	}

	var ret []*endorsev0.Platform_Entry
	state := "fw"

	// init PCR values
	for i := int32(1); i < numPCR; i++ {
		ent := &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_INIT,
			Pcr:  i,
		}
		// TCG PC Client Profile for TPM 2.0, Table 7
		if i <= 16 || i == 23 {
			ent.Digest = zeroPCR
		} else {
			ent.Digest = foxPCR
		}
		ret = append(ret, ent)
	}

	pcr0init := false
	for i := range log {
		ev := log[i]

		// DRTM init sequence
		if ev.RawEvent().Index == 0 && !pcr0init {
			pcr0 := append([]byte{}, zeroPCR...)

			// DRTM init
			noa, ok := ev.(eventlog.NoActionEvent)
			drtm_init := ok && len(noa.Data) == 17 && bytes.Equal(noa.Data[0:16], startupLocality)

			if drtm_init {
				pcr0[31] = noa.Data[len(noa.Data)-1]
			}

			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_INIT,
				Pcr:    0,
				Digest: pcr0,
			})

			if !drtm_init {
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_OPAQUE,
					Pcr:    0,
					Digest: ev.RawEvent().Digest,
				})
			}

			pcr0init = true
			continue
		}

		switch ev := ev.(type) {

		// UEFI boot app executed.
		case eventlog.UEFIBootServicesApplicationEvent:
			if ev.RawEvent().Index != 4 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			switch state {
			case "fw":
				state = "uki"
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_STBOOT_UKI,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: optionalValue(ev.RawEvent().Digest),
				})
			case "uki":
				state = "stboot"
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_STBOOT_AUTHENTIHASH,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: optionalValue(ev.RawEvent().Digest),
				})
			default:
				return nil, fmt.Errorf("too many applications")
			}

		// ExitBootServices()
		case eventlog.UEFIActionEvent:
			switch ev.Message {
			case "Exit Boot Services Returned with Success":
				if ev.RawEvent().Index != 5 {
					return nil, fmt.Errorf("unexpected pcr index")
				}
				switch state {
				case "uki":
					state = "stboot"
				case "stboot":
				default:
					return nil, fmt.Errorf("unexpected state")
				}
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OPAQUE,
				Pcr:    int32(ev.RawEvent().Index),
				Digest: ev.RawEvent().Digest,
			})

			// systemd-stub measurements
		case eventlog.IPLEvent:
			switch state {
			case "uki":
				if bytes.Equal(ev.RawEvent().Digest, linuxName[:]) {
					state = "uki:linux"
				} else if bytes.Equal(ev.RawEvent().Digest, initrdName[:]) {
					state = "uki:initrd"
				} else if bytes.Equal(ev.RawEvent().Digest, cmdlineName[:]) {
					state = "uki:cmdline"
				} else if bytes.Equal(ev.RawEvent().Digest, osrelName[:]) {
					state = "uki:osrel"
				}
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_OPAQUE,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: ev.RawEvent().Digest,
				})
			case "uki:linux":
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_STBOOT_LINUX,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: optionalValue(ev.RawEvent().Digest),
				})
				state = "uki"
			case "uki:initrd":
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_STBOOT_INITRD,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: optionalValue(ev.RawEvent().Digest),
				})
				state = "uki"
			case "uki:cmdline":
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_STBOOT_CMDLINE,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: optionalValue(ev.RawEvent().Digest),
				})
				state = "uki"
			case "uki:osrel":
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_STBOOT_OSREL,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: optionalValue(ev.RawEvent().Digest),
				})
				state = "uki"
			default:
				ret = append(ret, &endorsev0.Platform_Entry{
					Type:   endorsev0.Platform_Entry_OPAQUE,
					Pcr:    int32(ev.RawEvent().Index),
					Digest: ev.RawEvent().Digest,
				})
			}

			// stboot
		case eventlog.OspkgArchiveEvent:
			if state != "stboot" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 12 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OSPKG_ZIP,
				Pcr:    12,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "stboot:manifest"
		case eventlog.OspkgManifestEvent:
			if state != "stboot:manifest" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 12 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OSPKG_DESCRIPTOR,
				Pcr:    12,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "stboot:security"
		case eventlog.SecurityConfigEvent:
			if state != "stboot:security" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 13 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OSPKG_SECURITYCONFIG,
				Pcr:    13,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "stboot:root"
		case eventlog.SigningRootEvent:
			if state != "stboot:root" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 13 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OSPKG_SIGNINGROOT,
				Pcr:    13,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "stboot:https"
		case eventlog.HttpsRootEvent:
			if state != "stboot:https" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 13 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OSPKG_HTTPSROOTS,
				Pcr:    13,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "stboot:identity"
		case eventlog.UxIdentityEvent:
			if state != "stboot:identity" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 14 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_UX_IDENTITY,
				Pcr:    14,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "stboot:datachannel"
		case eventlog.DataChannelEvent:
			if state != "stboot:datachannel" {
				return nil, fmt.Errorf("unexpected state")
			}
			if ev.RawEvent().Index != 14 {
				return nil, fmt.Errorf("unexpected pcr index")
			}
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_DATA_CHANNEL,
				Pcr:    14,
				Digest: optionalValue(ev.RawEvent().Digest),
			})
			state = "tail"

			// all other measurements
		default:
			ret = append(ret, &endorsev0.Platform_Entry{
				Type:   endorsev0.Platform_Entry_OPAQUE,
				Pcr:    int32(ev.RawEvent().Index),
				Digest: ev.RawEvent().Digest,
			})
		}
	}

	switch state {
	case "stboot":
		// stboot measurements not in the log
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_OSPKG_ZIP,
			Pcr:  12,
		})
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_OSPKG_DESCRIPTOR,
			Pcr:  12,
		})
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_OSPKG_SECURITYCONFIG,
			Pcr:  13,
		})
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_OSPKG_SIGNINGROOT,
			Pcr:  13,
		})
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_OSPKG_HTTPSROOTS,
			Pcr:  13,
		})
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_UX_IDENTITY,
			Pcr:  14,
		})
		fallthrough
		// backwards compatibility with pre-datachannel measurements
	case "stboot:datachannel":
		ret = append(ret, &endorsev0.Platform_Entry{
			Type: endorsev0.Platform_Entry_DATA_CHANNEL,
			Pcr:  14,
		})

		fallthrough

	case "tail":
		return ret, nil

	default:
		return nil, fmt.Errorf("unexpected state: %s", state)
	}
}

func Reconstruct(pcr []int32, platform *endorsev0.Platform, stboot *endorsev0.Stboot, osPkg *endorsev0.OsPkg) (map[int32][]byte, error) {
	filled, err := FillTemplate(platform.Log, platform, stboot, osPkg)
	if err != nil {
		return nil, err
	}

	values := make(map[int32][]byte)
	for _, i := range pcr {
		values[i] = nil
	}

	for i := range filled {
		entry := &filled[i]
		if _, ok := values[entry.Pcr]; !ok {
			continue
		}

		switch entry.Type {
		case endorsev0.Platform_Entry_INIT:
			values[entry.Pcr] = entry.Digest

		default:
			sum := sha256.Sum256(append(values[entry.Pcr], entry.Digest...))
			values[entry.Pcr] = sum[:]
		}
	}

	return values, nil
}

func FillTemplate(log []*endorsev0.Platform_Entry, platform *endorsev0.Platform, stboot *endorsev0.Stboot, osPkg *endorsev0.OsPkg) ([]endorsev0.Platform_Entry, error) {
	filled := make([]endorsev0.Platform_Entry, len(log))

	for i, entry := range platform.Log {
		filled[i].Pcr = entry.Pcr
		filled[i].Type = entry.Type

		var d []byte
		switch entry.Type {
		case endorsev0.Platform_Entry_OSPKG_HTTPSROOTS:
			h := sha256.New()
			for _, root := range stboot.HttpsRoots {
				h.Write(root)
			}
			filled[i].Digest = h.Sum(nil)
			continue

		case endorsev0.Platform_Entry_OPAQUE:
			fallthrough
		case endorsev0.Platform_Entry_INIT:
			d = entry.Digest
		case endorsev0.Platform_Entry_STBOOT_UKI:
			d = stboot.Uki
		case endorsev0.Platform_Entry_STBOOT_LINUX:
			d = stboot.Linux
		case endorsev0.Platform_Entry_STBOOT_INITRD:
			d = stboot.Initrd
		case endorsev0.Platform_Entry_STBOOT_CMDLINE:
			d = stboot.Cmdline
		case endorsev0.Platform_Entry_STBOOT_OSREL:
			d = stboot.Osrel
		case endorsev0.Platform_Entry_STBOOT_AUTHENTIHASH:
			d = stboot.Authentihash
		case endorsev0.Platform_Entry_OSPKG_ZIP:
			sum := sha256.Sum256(osPkg.Zip)
			d = sum[:]
		case endorsev0.Platform_Entry_OSPKG_DESCRIPTOR:
			sum := sha256.Sum256(osPkg.Descriptor_)
			d = sum[:]
		case endorsev0.Platform_Entry_OSPKG_SECURITYCONFIG:
			sum := sha256.Sum256(stboot.SecurityConfig)
			d = sum[:]
		case endorsev0.Platform_Entry_OSPKG_SIGNINGROOT:
			sum := sha256.Sum256(stboot.SigningRoot)
			d = sum[:]
		case endorsev0.Platform_Entry_UX_IDENTITY:
			sum := sha256.Sum256([]byte(platform.UxIdentity))
			d = sum[:]
		case endorsev0.Platform_Entry_DATA_CHANNEL:
			sum := sha256.Sum256([]byte(platform.DataChannel))
			d = sum[:]
		default:
			return nil, ErrFormat
		}
		filled[i].Digest = d
	}

	return filled, nil
}

func PrintAndValidate(log []eventlog.Event) {
	ll, _ := eventlog.ParseEvents(log)
	for _, entry := range ll {
		var expected []byte

		fmt.Printf("PCR[%02d] %s: ", entry.RawEvent().Index, entry.RawEvent().Type)
		switch entry := entry.(type) {
		case eventlog.SigningRootEvent:
			sum := sha256.New()
			for _, cert := range entry.Certificates {
				sum.Write(cert.Raw)
				if cert.Subject.CommonName == "" {
					fmt.Printf("%x, ", sha256.Sum256(cert.Raw))
				} else {
					fmt.Printf("%s, ", cert.Subject.CommonName)
				}
			}
			fmt.Printf("\n")
			expected = sum.Sum(nil)

		case eventlog.HttpsRootEvent:
			sum := sha256.New()
			for _, cert := range entry.Certificates {
				sum.Write(cert.Raw)
				fmt.Printf("%s, ", cert.Subject.CommonName)
			}
			fmt.Printf("\n")
			expected = sum.Sum(nil)

		case eventlog.OspkgArchiveEvent:
			fmt.Printf("%s\n", entry.Source)

		case eventlog.OspkgManifestEvent:
			fmt.Printf("%s\n", entry.Json)
			sum := sha256.Sum256([]byte(entry.Json))
			expected = sum[:]

		case eventlog.SecurityConfigEvent:
			fmt.Printf("%s\n", entry.Json)
			sum := sha256.Sum256([]byte(entry.Json))
			expected = sum[:]

		case eventlog.UxIdentityEvent:
			fmt.Printf("%s\n", entry.Identity)
			sum := sha256.Sum256([]byte(entry.Identity))
			expected = sum[:]

		case eventlog.DataChannelEvent:
			sum := sha256.New()
			for _, cert := range entry.Certificates {
				sum.Write(cert.Raw)
				if cert.Subject.CommonName == "" {
					fmt.Printf("%x, ", sha256.Sum256(cert.Raw))
				} else {
					fmt.Printf("%s, ", cert.Subject.CommonName)
				}
			}
			fmt.Printf("\n")
			expected = sum.Sum(nil)

		default:
			fmt.Printf("%x\n", entry.RawEvent().Digest)
		}

		if len(expected) > 0 && !bytes.Equal(entry.RawEvent().Digest, expected) {
			fmt.Printf("==== digest mismatch ====\n")
		}
	}
}

func AnalyseFailure(ctx context.Context, platform *endorsev0.Platform, stboot *endorsev0.Stboot, osPkg *endorsev0.OsPkg, q *quotev0.Response) error {
	log := zerolog.Ctx(ctx)

	// UEFI event log
	if len(q.UefiLog) == 0 {
		return errors.New("no uefi log in quote")
	}
	l, err := eventlog.ParseEventLog(q.UefiLog)
	if err != nil {
		return err
	}

	recvLog := l.Events(eventlog.HashSHA256)

	if len(q.StbootLog) > 0 {
		l, err := eventlog.ParseEventLog(q.StbootLog)
		if err != nil {
			return err
		}
		recvLog = append(recvLog, l.Events(eventlog.HashSHA256)...)
	} else {
		log.Warn().Msg("no stboot log in quote")
	}

	recvTmpl, err := MakeTemplate(recvLog, false)
	if err != nil {
		return err
	}

	endrTmpl, err := FillTemplate(platform.Log, platform, stboot, osPkg)
	if err != nil {
		return err
	}

	var endrStr, recvStr string
	for i := range endrTmpl {
		entry := &endrTmpl[i]
		endrStr += fmt.Sprintf("PCR[%02d] %20s %x\n", entry.Pcr, entry.Type, entry.Digest)
	}
	for _, entry := range recvTmpl {
		recvStr += fmt.Sprintf("PCR[%02d] %20s %x\n", entry.Pcr, entry.Type, entry.Digest)
	}

	dmp := diffmatchpatch.New()
	diffs := dmp.DiffCleanupSemantic(dmp.DiffMain(endrStr, recvStr, false))

	// summarize long equal lines
	for i := range diffs {
		diff := &diffs[i]
		switch diff.Type {
		case diffmatchpatch.DiffEqual:
			lns := strings.Split(diff.Text, "\n")

			if i > 0 && len(lns) > 4 {
				diff.Text = fmt.Sprintf("%s\n%s\n   %d entries...\n%s\n%s", lns[0], lns[1], len(lns)-4, lns[len(lns)-2], lns[len(lns)-1])
			} else if i == 0 && len(lns) > 2 {
				diff.Text = fmt.Sprintf("%d entries...\n%s\n%s", len(lns)-2, lns[len(lns)-2], lns[len(lns)-1])
			}

		default:
		}
	}
	fmt.Println(dmp.DiffPrettyText(dmp.DiffCleanupSemantic(diffs)))

	return nil
}
