package attestation

import (
	"fmt"
	"os"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
	"google.golang.org/protobuf/proto"

	"git.glasklar.is/system-transparency/core/stauth/endorsev0"
	"git.glasklar.is/system-transparency/core/stauth/metadata"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
)

func TestEndorseStboot(t *testing.T) {
	st, err := NewStboot("../testdata/stboot.iso")
	assert.NoError(t, err)

	var en endorsev0.Endorsement
	en.Endorsement = &endorsev0.Endorsement_Stboot{
		Stboot: st,
	}

	buf1, err := proto.Marshal(&en)
	assert.NoError(t, err)

	buf2, err := os.ReadFile("../testdata/stboot.stboot.pb")
	assert.NoError(t, err)

	assert.Equal(t, buf1, buf2)
}

func TestEndorseOsPkg(t *testing.T) {
	ospkg, err := NewOsPkg("../testdata/os-pkg-example-ubuntu20.zip", "../testdata/os-pkg-example-ubuntu20.json")
	assert.NoError(t, err)

	var en endorsev0.Endorsement
	en.Endorsement = &endorsev0.Endorsement_OsPkg{
		OsPkg: ospkg,
	}

	buf1, err := proto.Marshal(&en)
	assert.NoError(t, err)

	buf2, err := os.ReadFile("../testdata/os-pkg-example-ubuntu20.ospkg.pb")
	assert.NoError(t, err)

	assert.Equal(t, buf1, buf2)
}

func TestEndorse(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests in short mode.")
	}

	type test struct {
		id       string
		uefilog  string
		metadata string
		platform string
	}

	tests := []test{
		{
			"test.example.com",
			"../testdata/log-nontxt.binary",
			"../testdata/metadata-nontxt.binary",
			"../testdata/log-nontxt.platform.pb",
		}, {
			"",
			"../testdata/log-noid.binary",
			"../testdata/metadata-noid.binary",
			"../testdata/log-noid.platform.pb",
		},
	}

	for _, test := range tests {
		for _, withstlog := range []bool{false, true} {
			t.Run(fmt.Sprintf("log=%s, withstlog=%v", test.uefilog, withstlog), func(t *testing.T) {
				uefilog, err := os.ReadFile(test.uefilog)
				assert.NoError(t, err)

				var stlog []byte
				if withstlog {
					mbin, err := os.ReadFile(test.metadata)
					assert.NoError(t, err)
					mockFs := make(fstest.MapFS)
					mockFs["dev/pmem0"] = &fstest.MapFile{Data: mbin, Mode: 0644}
					stlog, err = metadata.Read(metadata.EventLog, mockFs)
					assert.NoError(t, err)
				}

				plt, err := NewPlatform(uefilog, stlog, tcg.DefaultSRKTemplate, tcg.DefaultAIKTemplate, nil, test.id, nil)
				assert.NoError(t, err)

				var en1, en2 endorsev0.Endorsement
				en1.Endorsement = &endorsev0.Endorsement_Platform{
					Platform: plt,
				}

				buf1, err := proto.Marshal(&en1)
				assert.NoError(t, err)

				buf2, err := os.ReadFile(test.platform)
				assert.NoError(t, err)
				err = proto.Unmarshal(buf2, &en2)
				assert.NoError(t, err)
				en2.GetPlatform().AikQname = plt.AikQname
				en2.GetPlatform().AikPublic = plt.AikPublic
				en2.GetPlatform().AikPrivate = plt.AikPrivate
				buf2, err = proto.Marshal(&en2)
				assert.NoError(t, err)

				assert.Equal(t, buf1, buf2)
			})
		}
	}
}
