package attestation

import (
	"context"
	"crypto/x509"
	"errors"
	"io"
	"io/fs"
	"strings"

	"git.glasklar.is/system-transparency/core/stauth/efi"
	"git.glasklar.is/system-transparency/core/stauth/tcg"
	"github.com/google/go-tpm/legacy/tpm2"
	"github.com/google/uuid"
)

var (
	ErrUnprovisioned   = errors.New("no identity provisioned")
	ErrInvalidIdentity = errors.New("invalid identity")

	UxIdentityNV tpm2.NVPublic = tpm2.NVPublic{
		NVIndex:    0x01_420001,
		NameAlg:    tpm2.AlgSHA256,
		Attributes: tpm2.AttrAuthRead | tpm2.AttrAuthWrite | tpm2.AttrNoDA,
		AuthPolicy: nil,
		DataSize:   253,
	}

	// EFI variables for the data channel public and private keys
	VaribleGuid       = uuid.MustParse("f401f2c1-b005-4be0-8cee-f2e5945bcbe7")
	DataChannelPublic = "DataChannelPublic"
)

func DataChannel(fsys fs.FS) (*x509.Certificate, error) {
	// read data channel public key from EFI variables
	_, pub, err := efi.Variable(VaribleGuid, DataChannelPublic, fsys)
	if err == efi.ErrVarNotExist {
		return nil, ErrUnprovisioned
	} else if err != nil {
		return nil, err
	}

	cert, err := x509.ParseCertificate(pub)
	if err != nil {
		return nil, err
	}

	return cert, nil
}

func Identity(ctx context.Context, conn io.ReadWriteCloser) (string, error) {
	err := tcg.NvHandle(conn, UxIdentityNV.NVIndex)
	if err == tcg.ErrNoSuchHandle {
		return "", ErrUnprovisioned
	}
	if err != nil {
		return "", err
	}

	// read identity from TPM
	rawid, err := tpm2.NVReadEx(conn, UxIdentityNV.NVIndex, UxIdentityNV.NVIndex, "", 0)
	if err != nil {
		return "", err
	}

	return strings.Trim(string(rawid), "\x00"), nil
}

func RemoveIdentity(ctx context.Context, conn io.ReadWriteCloser) error {
	err := tcg.NvHandle(conn, UxIdentityNV.NVIndex)
	if err == tcg.ErrNoSuchHandle {
		return nil
	}

	// delete the NV index
	err = tpm2.NVUndefineSpace(conn, "", tpm2.HandleOwner, UxIdentityNV.NVIndex)
	if err != nil {
		return err
	}

	return nil
}

func OverwriteIdentity(ctx context.Context, conn io.ReadWriteCloser, identity string) error {
	if len(identity) == 0 || len(identity) > int(UxIdentityNV.DataSize) {
		return ErrInvalidIdentity
	}

	err := tcg.NvHandle(conn, UxIdentityNV.NVIndex)
	if err == tcg.ErrNoSuchHandle {
		err = tpm2.NVDefineSpace(conn, tpm2.HandleOwner, UxIdentityNV.NVIndex, "", "", UxIdentityNV.AuthPolicy, UxIdentityNV.Attributes, UxIdentityNV.DataSize)
		if err != nil {
			return err
		}
	}

	rawid := make([]byte, UxIdentityNV.DataSize)
	copy(rawid, identity)

	// write identity to TPM
	err = tpm2.NVWrite(conn, UxIdentityNV.NVIndex, UxIdentityNV.NVIndex, "", rawid, 0)
	if err != nil {
		return err
	}

	return nil
}
