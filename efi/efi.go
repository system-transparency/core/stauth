package efi

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"path/filepath"

	"github.com/google/uuid"
)

const (
	// Path to the EFI variables.
	EfivarfsMountPoint = "sys/firmware/efi/efivars"
)

var (
	// ErrVarNotExist is returned when the EFI variable does not exist.
	ErrVarNotExist = fmt.Errorf("variable does not exist")
	// ErrVarPermission is returned when the EFI variable cannot be read due to
	// permissions.
	ErrVarPermission = fmt.Errorf("variable cannot be read due to permissions")
)

func Variable(guid uuid.UUID, name string, fsys fs.FS) (uint32, []byte, error) {
	path := filepath.Join(EfivarfsMountPoint, fmt.Sprintf("%s-%s", name, guid.String()))
	f, err := fsys.Open(path)
	if errors.Is(err, fs.ErrNotExist) {
		return 0, nil, ErrVarNotExist
	} else if errors.Is(err, fs.ErrPermission) {
		return 0, nil, ErrVarPermission
	} else if err != nil {
		return 0, nil, err
	}
	defer f.Close()

	var attrs uint32
	if err := binary.Read(f, binary.LittleEndian, &attrs); err != nil {
		if err == io.EOF {
			return 0, nil, ErrVarNotExist
		}
		return 0, nil, err
	}

	data, err := io.ReadAll(f)
	if err != nil {
		return 0, nil, err
	}
	return attrs, data, nil
}
